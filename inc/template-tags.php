<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Rokjedna
 */

if ( ! function_exists( 'rokjedna_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function rokjedna_posted_on( $post = NULL ) {

		if ( $post == NULL ) {
			$post = get_post();
		}

		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C, $post->ID ) ),
			esc_html( get_the_date("j. n. Y", $post->ID) )
		);

		/*
		$posted_on = sprintf(
			esc_html_x( 'Posted on %s', 'post date', "rokjedna" ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);
		*/

		$posted_on = $time_string;

		echo '<span class="rj-post-meta-item rj-post-meta-item_posted-on">' . $posted_on . '</span>';

	}
endif;

if ( ! function_exists( 'rokjedna_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function rokjedna_posted_by( $post = NULL ) {

		if ( $post == NULL ) {
			$post = get_post();
		}

		$post_author_id = get_post_field( 'post_author', $post->ID );
		$user = get_user_by( 'ID', $post_author_id );

		$byline = sprintf(
			'%s',
			'<span class="author vcard">' . esc_html( $user->display_name ) . '</span>'
		);

		echo '<span class="rj-post-meta-item rj-post-meta-item_byline"> ' . $byline . '</span>';

	}
endif;

function rj_post_categories( $post = NULL ){
	if ($post == NULL) {
		$post = get_post();
	}
	if ( gettype( $post ) == "integer" ) {
		$post = get_post( $post );
	}
	$categories_list = get_the_category_list( esc_html__( ', ', 'rokjedna' ), "single", $post->ID );
	if ( $categories_list ) {
		/* translators: 1: list of categories. */
		printf( '<span class="cat-links">' . esc_html__( '%1$s', 'rokjedna' ) . '</span>', $categories_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}
	wp_reset_query();
}

if ( ! function_exists( 'rokjedna_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function rokjedna_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'rokjedna' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'rokjedna' ) . '</span>', $categories_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'rokjedna' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'rokjedna' ) . '</span>', $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'rokjedna' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'rokjedna' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'rokjedna_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function rokjedna_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

			<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
				<?php
					the_post_thumbnail(
						'post-thumbnail',
						array(
							'alt' => the_title_attribute(
								array(
									'echo' => false,
								)
							),
						)
					);
				?>
			</a>

			<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'wp_body_open' ) ) :
	/**
	 * Shim for sites older than 5.2.
	 *
	 * @link https://core.trac.wordpress.org/ticket/12563
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
endif;


if ( ! function_exists( 'rokjedna_image_url' ) ) :
	function rokjedna_image_url($url){

		return get_template_directory_uri() . "/src/img/" . $url;

	}
endif;



if ( ! function_exists('rj_site_title') ):

	function rj_site_title(){

		$el = "div";
		if ( is_front_page() && is_home() ) {
			$el = "h1";
		}

		$letters = rj_is_blog() ? __("Students to students","rokjedna") : get_bloginfo("name");

		$url = esc_url( home_url( "/" ) );

		if ( rj_is_blog() ) {
			
			$root_cat = get_theme_mod( "rj_blog_category" );

			if ( $root_cat ) {
				$url = get_category_link( $root_cat );
			}

		}


		$output = "<".$el." id='rjSiteName' class='rj-site-name rj-h-part rj-h-part_bottom'>";
		$output .= "<a href='".$url."' class='rj-site-name-content' aria-label='".translate("Link to the homepage of the site","rokjedna")."'>";
		$output .= $letters;
		$output .= "</a>";
		$output .= "</".$el.">";

		return $output;

	}

endif;







/**
 * Print the rj-post-content_* classes of a post or page
 */






// helper function to find a menu item in an array of items


