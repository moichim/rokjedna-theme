<?php

class Walker_Expandable_Menu extends Walker_Nav_Menu {

  private $pointer = 0;

  function start_lvl(&$output, $depth=0, $args=array()) {

      parent::start_lvl($output, $depth,$args);
  }

  function end_lvl( &$output, $depth = 0, $args = array() ) {
      parent::end_lvl( $output, $depth = 0, $args);
  }

  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    if ( $item->post_parent == 0 && $item->object == "page" ) {

      if ( wp_get_post_parent_id( $item->object_id ) == false ) {

        if ($section = rj_page_section_slug((int)$item->object_id) ) {

          array_push( $item->classes, "section-color-" . $section );

        }

      }

    }

    parent::start_el( $output, $item, $depth = 0, $args, $id );

  }

  function end_el( &$output, $item, $depth = 0, $args = array() ) {

    $needle = '<a href="'.$item->url.'"';
    $haystack = '<a id="rj-m-'.$item->ID.'" href="'.$item->url.'"';
    $output = str_replace($needle, $haystack, $output);

    if ( in_array( "menu-item-has-children", $item->classes ) ) {

      $searched = 'menu-item-'.$item->ID;
      $replace = $searched . " rj-menu-item_closed";
      $output = str_replace( $searched, $replace, $output );

      $searched2 = 'rj-menu-item_closed';
      $replaced2 = 'rj-menu-item_closed" aria-expanded="false';
      $output = str_replace( $searched2, $replaced2, $output );

      // $searched3 = '<a href="'.$item->url.'">';
      // $replaced3 = '<a id="rj_m_" href="'.$item->url.'"';

      $output = str_replace( $item->title . "</a>", $item->title."</a><button class='rj-menu-item-expander' aria-label='Rozbalit obsah sekce ".$item->title."'></button>", $output );

    }

      parent::end_el( $output, $item, $depth, $args);

  }

  function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

      parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );

  }
}


class Walker_Main_Menu extends Walker_Nav_Menu {

  private $pointer = 0;

  function start_lvl(&$output, $depth=0, $args=array()) {

      parent::start_lvl($output, $depth,$args);
  }

  function end_lvl( &$output, $depth = 0, $args = array() ) {
      parent::end_lvl( $output, $depth = 0, $args);
  }

  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    if ( $item->post_parent == 0 && $item->object == "page" ) {

      if ( wp_get_post_parent_id( $item->object_id ) == false ) {

        if ($section = rj_page_section_slug((int)$item->object_id) ) {

          array_push( $item->classes, "rj-m-section-color-" . $section );

          ?>
          <style>
            .rj-m-section-color-<?= $section ;?> a:hover,
            .rj-m-section-color-<?= $section ;?> a:focus,
            .rj-m-section-color-<?= $section ;?> a:active
            {
              background-color: <?= rj_page_section_color( (int)$item->object_id ); ?> !important;
              color: <?= rj_page_section_color_text( (int)$item->object_id ); ?> !important;
            }

            .rj-h-part_main-menu ul.rj-menu_main .current-menu-ancestor.rj-m-section-color-<?= $section ;?> a,
            .rj-h-part_main-menu ul.rj-menu_main .current-menu-item.rj-m-section-color-<?= $section ;?> a {
              color: <?= rj_page_section_color( (int)$item->object_id ); ?>;
            }
            .rj-h-part_main-menu ul.rj-menu_main .current-menu-ancestor.rj-m-section-color-<?= $section ;?> a:hover,
            .rj-h-part_main-menu ul.rj-menu_main .current-menu-item.rj-m-section-color-<?= $section ;?> a:hover {
              color: <?= rj_page_section_color_text( (int)$item->object_id ); ?> !important;
            }
          </style>
          <?php

        }

      }

    }

    parent::start_el( $output, $item, $depth = 0, $args, $id );
    
  }

  function end_el( &$output, $item, $depth = 0, $args = array() ) {

    $needle = '<a href="'.$item->url.'"';
    $haystack = '<a id="rj-m-'.$item->ID.'" href="'.$item->url.'" class="rj-menu-link"';
    $output = str_replace($needle, $haystack, $output);

    parent::end_el( $output, $item, $depth, $args);

  }

  function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

      parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );

  }
}

class Selective_Walker extends Walker_Nav_Menu
{
    var $menu_item;

    function __construct($menu_item_id = false) {

        $this->menu_item = $menu_item_id;
    }


    // Don't start the top level
    function start_lvl(&$output, $depth=0, $args=array()) {
        if( 0 == $depth )
            return;
        parent::start_lvl($output, $depth,$args);
    }

    // Don't end the top level
    function end_lvl(&$output, $depth=0, $args=array()) {
        if( 0 == $depth )
            return;
        parent::end_lvl($output, $depth,$args);
    }

    // Don't print top-level elements
    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0 ) {
        if( 0 == $depth && !$this->menu_item )
            return;
        parent::start_el($output, $item, $depth, $args, $id );
    }

    function end_el(&$output, $item, $depth=0, $args=array()) {
       if( 0 == $depth && !$this->menu_item )
          return;
        parent::end_el($output, $item, $depth, $args);
    }

    // Only follow down one branch
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

         // Check if element as a 'current element' class
         $current_element_markers = array( 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor' );
         $current_class = array_intersect( $current_element_markers, explode( " ", $element->classes ) );

         if( !$this->menu_item)
         {                      
            // If element has a 'current' class, it is an ancestor of the current element
            $ancestor_of_current = !empty($current_class);       

            // If this is a top-level link and not the current, or ancestor of the current menu item - stop here.
            if ( 0 == $depth && !$ancestor_of_current)
                return;

            parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
         }
         else
         {       
            if ( $this->menu_item != $element->menu_item_parent )
                return;

             parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
         }
    }
}