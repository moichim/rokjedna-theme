<?php
/**
 * Global attributes for all posts
 * 
 * @package Rokjedna
 */

 /**
 * TEMPLATE TAGS
 */
if ( ! function_exists( "rj_post_gutter_classes" ) ) :

	function rj_post_gutter_classes( $post=NULL ){

		// Load the current post if no parameter provided
		if ($post == NULL) { 
			$post = get_post(); 
		}

		// Load the post object if an integer is provided
		if ( gettype($post) == "integer" ) {
			$post = get_post($post);
		}

		// If the post is loaded properly, proceed
		if ( gettype($post) == "object" && $post != NULL ) {

			$raw = get_post_meta($post->ID,"post_paddings", true);

			foreach ( $raw as $element ) {

				switch ( $element ) {

					case "horizontal":
						print( " rj-post-content_ph" );
						break;

					case "vertical":
						print( " rj-post-content_pv" );
						break;

					case "stretch":
						print( " rj-post-content_stretched" );
						break;
					case "gutenberg":
						print( " rj-post-content_gutenberg" );
						break;

				}

			}

		}

	}

endif;