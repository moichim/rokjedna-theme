<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

?>

<article id="post-<?php the_ID(); ?>" class="rj-archive-item" role="listitem" aria-atomic="true">
	<a class="rj-archive-item-wrap" href="<?= get_permalink(); ?>">
		<header class="entry-header">

      <?php the_post_thumbnail("thumbnail"); ?>

			<?php the_title( sprintf( '<h2 class="rj-post-list-item-title">', esc_url( get_permalink() ) ), '</h2>' ); ?>

			<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php
				rokjedna_posted_on();
				?>
			</div><!-- .entry-meta -->
			<?php endif; ?>

			<?php if ( $slug && $title = rj_page_section_title() != get_the_title() ) : ?>
				<div class="rj-post-list-item-footer">
					<span class="rj-post-list-item-label">V sekci</span> <span class="rj-page-section-name"><?= rj_page_section_title(); ?><span>
				</div>
			<?php endif; ?>

		</header>

		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div>

    <div class="rj-archive-detail-readmore"><?= __("Read more","rokjedna"); ?></div>

  </a>

	
</article><!-- #post-<?php the_ID(); ?> -->