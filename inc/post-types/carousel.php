<?php

/**
 * Custom post type Checklist
 * 
 * Is used for outputting the homapege calendar.
 * 
 * @package Rokjedna
 */

 /**
  * DEFINITION
  */
function rj_carousel_register() {

	$labels = array(
		'name'                  => _x( 'Frontpage carousels', 'Post Type General Name', 'rokjedna' ),
		'singular_name'         => _x( 'Frontpage carousel', 'Post Type Singular Name', 'rokjedna' ),
		'menu_name'             => __( 'Carousel', 'rokjedna' ),
		'name_admin_bar'        => __( 'Carousel', 'rokjedna' ),
		'archives'              => __( 'Item Archives', 'rokjedna' ),
		'attributes'            => __( 'Item Attributes', 'rokjedna' ),
		'parent_item_colon'     => __( 'Parent Item:', 'rokjedna' ),
		'all_items'             => __( 'All Carousels', 'rokjedna' ),
		'add_new_item'          => __( 'Add New Carousel', 'rokjedna' ),
		'add_new'               => __( 'Add New', 'rokjedna' ),
		'new_item'              => __( 'New Carousel', 'rokjedna' ),
		'edit_item'             => __( 'Edit Carousel', 'rokjedna' ),
		'update_item'           => __( 'Update Carousel', 'rokjedna' ),
		'view_item'             => __( 'View Carousel', 'rokjedna' ),
		'view_items'            => __( 'View Carousels', 'rokjedna' ),
		'search_items'          => __( 'Search Carousels', 'rokjedna' ),
		'not_found'             => __( 'Not found', 'rokjedna' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'rokjedna' ),
		'featured_image'        => __( 'Featured Image', 'rokjedna' ),
		'set_featured_image'    => __( 'Set featured image', 'rokjedna' ),
		'remove_featured_image' => __( 'Remove featured image', 'rokjedna' ),
		'use_featured_image'    => __( 'Use as featured image', 'rokjedna' ),
		'insert_into_item'      => __( 'Insert into item', 'rokjedna' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'rokjedna' ),
		'items_list'            => __( 'Items list', 'rokjedna' ),
		'items_list_navigation' => __( 'Items list navigation', 'rokjedna' ),
		'filter_items_list'     => __( 'Filter items list', 'rokjedna' ),
	);
	$args = array(
		'label'                 => __( 'Carousel', 'rokjedna' ),
		'description'           => __( 'Carousel', 'rokjedna' ),
		'labels'                => $labels,
		'supports'              => array("title"),
		// 'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 50,
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => false,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'menu_icon'             => "dashicons-embed-generic"
	);
	register_post_type( 'carousel', $args );

}

add_action( 'init', 'rj_carousel_register', 0 );



/**
 * CUSTOM FUELDS
 */
function rj_register_carousel_metaboxes() {

  $cmb = new_cmb2_box( array(
		'id'            => 'carousel_fields',
		'title'         => esc_html__( 'Carousel Contents', 'rokjedna' ),
		'object_types'  => array( 'carousel' ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'side',
		// 'priority'   => 'high',
		// 'show_names' => false, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		// 'classes'    => 'extra-class', // Extra cmb2-wrap classes
		// 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
  ) );

  $cmb->add_field( array(
		'name'    => 'Podtitul',
		'desc'    => 'Druhá linka se zobrazí pod nadpisem',
		// 'default' => 'standard value (optional)',
		'id'      => 'carousel_subtitle',
		'type'    => 'text',
		"attributes" => array(
			"type" => "text",
			"required" => true
		),
	) );

	$cmb->add_field( array(
		'name'    => 'Obrázek',
		'desc'    => 'Nahrajte obrázek ve formátu JPG či PNG',
		'id'      => 'carousel_image',
		'type'    => 'file',
		// Optional:
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
		'text'    => array(
			'add_upload_file_text' => "Nahrát obrázek" // Change upload button text. Default: "Add or Upload File"
		),
		// query_args are passed to wp.media's library query.
		'query_args' => array(
			// 'type' => 'application/pdf', // Make library only display PDFs.
			// Or only allow gif, jpg, or png images
			'type' => array(
			//  	'image/gif',
			 	'image/jpeg',
			 	'image/png',
			),
		),
		'preview_size' => 'large', // Image size to use when previewing in the admin.
	) );

	$cmb->add_field( array(
		'name'    => 'URL',
		'desc'    => 'Odkaz, kam carousel povede',
		// 'default' => 'standard value (optional)',
		'id'      => 'carousel_link',
		'type'    => 'text_url',
	) );


}

add_action( 'cmb2_admin_init', 'rj_register_carousel_metaboxes' );


/**
 * FRONTPAGE MANAGER
 */


class FrontCarouselManager {
	private $ready;
	private $count;
	private $posts;
	private $args;
	private $query;
	
	

	function __construct(){

		$this->ready = false;
		$this->posts = array();
		$this->count = 0;

		$this->args = array(
			"post_type" => "carousel",
			"posts_per_page" => 3,
			"orderby" => "date",
			"order" => "DESC",
    );

		$this->query = new WP_Query( $this->args );

		if ( $this->query->have_posts() ) {

			$this->ready = true;
			$this->count = count($this->query->posts);

			while ( $this->query->have_posts() ) {

				$this->query->the_post();

				$post = get_post();

				$meta = get_post_meta($post->ID,"",true);
				$post->meta = $meta;

				array_push( $this->posts, $post );

			}
		}

		wp_reset_postdata();

		// $this->dev( $this );
	}

	private function dev( $input ){
		print "<pre>";
		print_r($input);
		print "</pre>";
	}

	public function render() {

		if ( $this->ready ) {
			$this->render_dots();
		}

	}

	private function get_slide_background( $post ) {

		return "style=background-image:url('".$post->meta["carousel_image"][0]."')";

	}

	public function render_dots() {

		foreach ( $this->posts as $key => $post ) {
			?>
				<li data-target="#carouselFrontIndicators" data-slide-to="<?= $key; ?>"<?php if ($key==0) :?> class="active"<?php endif; ?>></li>
			<?php
		}

	}

	public function render_slides() {
		foreach ( $this->posts as $key => $post ) {

			// Assamble the link
			$link = false;

			if ( !empty( $post->meta["carousel_link"] ) ) {
				if ( count( $post->meta["carousel_link"] ) > 0 ) {
					$link = $post->meta["carousel_link"][0];
				}
			}

			// Assamble the element
			$element = $link ? "a" : "span";

			// Get the subtitle
			$subtitle = false;

			if ( !empty( $post->meta["carousel_subtitle"] ) ) {
				if ( count( $post->meta["carousel_subtitle"] ) > 0 ) {
					$subtitle = $post->meta["carousel_subtitle"][0];
				}
			}

			?>
				<article class="carousel-item rj-carousel-item rj-carousel-item_front <?php if ($key==0) :?> active<?php endif; ?>" <?= $this->get_slide_background($post); ?>>
					
					<div class="rj-carousel-item-text">
						<h2 class="rj-carousel-item-title">
							<<?= $element; ?> href="<?= $link; ?>"><?= get_the_title($post); ?></<?= $element; ?>>
            </h2>
						<?php if ($subtitle) :?>
            <div class="rj-carousel-item-excerpt">
							<?= $subtitle; ?>
						</div>
						<?php endif; ?>
					</div>
				</article>
			<?php
		}
	}
}

function rj_front_carousel() {
	$ch = new FrontCarouselManager();
	$ch->render();
}