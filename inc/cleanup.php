<?php
/**
 * The cleanup code for the template
 * Big thanks to Reverie Theme & Graphique.cz
 * @package Rokjedna
 */

// Start all the functions
add_action( 'after_setup_theme','rokjedna_cleanup' );

if( !function_exists( 'rokjedna_cleanup' ) ) {
	
	function rokjedna_cleanup() {
	    
	  // Start head cleanup
	  add_action('init', 'rokjedna_head_cleanup');

	  // remove pesky injected css for recent comments widget
	  // add_filter( 'wp_head', 'rokjedna_remove_wp_widget_recent_comments_style', 1 );	    
	  // clean up comment styles in the head
	  // add_action('wp_head', 'rokjedna_remove_recent_comments_style', 1);

	  // remove WP version from RSS
	  add_filter('the_generator', 'rokjedna_rss_version');
	    
	  // clean up gallery output in wp
	  // add_filter('gallery_style', 'wpf_gallery_style');

	  // Remove open sans font from head
	  add_action('wp_enqueue_scripts', 'remove_wp_open_sans');	
		// Comment below to unremove font from admin
		add_action('admin_enqueue_scripts', 'remove_wp_open_sans');

		// Remove and diable JSON API
		// add_action( 'init', 'disable_json_api' );
		// add_action( 'init', 'remove_json_api' );

	} // End of rokjedna_init() 
}

/**
 * Disable JSON API 
 */
function disable_json_api () {

  // Filters for WP-API version 1.x
  add_filter('json_enabled', '__return_false');
  add_filter('json_jsonp_enabled', '__return_false');

  // Filters for WP-API version 2.x
  add_filter('rest_enabled', '__return_false');
  add_filter('rest_jsonp_enabled', '__return_false');

}


/**
 * Remove JSON API 
 */
function remove_json_api () {

    // Remove the REST API lines from the HTML Header
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

    // Remove the REST API endpoint.
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );

    // Turn off oEmbed auto discovery.
    add_filter( 'embed_oembed_discover', '__return_false' );

    // Don't filter oEmbed results.
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

    // Remove oEmbed discovery links.
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );

}


/**
 * Remove CDN Open Sans font 
 */
if ( !function_exists( 'remove_wp_open_sans' ) ) {

	function remove_wp_open_sans() {
		wp_deregister_style( 'open-sans' );
		wp_register_style( 'open-sans', false );
	}
}

/**
 * Cleanup of WP_HEAD
 */
if( !function_exists( 'rokjedna_head_cleanup ' ) ) {
	
	function rokjedna_head_cleanup() {
		
		// category feeds
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		// post and comment feeds
		remove_action( 'wp_head', 'feed_links', 2 );
		// EditURI link
		remove_action( 'wp_head', 'rsd_link' );
		// windows live writer
		remove_action( 'wp_head', 'wlwmanifest_link' );
		// index link
		remove_action( 'wp_head', 'index_rel_link' );
		// previous link
		// remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
		// start link
		// remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
		// links for adjacent posts
		// remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
		// WP version
		remove_action( 'wp_head', 'wp_generator' );
      	// Shortlink in head        
	  	remove_action('wp_head', 'wp_shortlink_wp_head'); 
	  	// Removes emoji JS
	  	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );  
	  	// Removes emoji CSS
    	remove_action( 'wp_print_styles', 'print_emoji_styles' );
    	// Removes generator name from RSS
	  	add_filter('the_generator', '__return_false');    
	  	// Removes cookies
	  	remove_action('set_comment_cookies', 'wp_set_comment_cookies');    

		// remove WP version from css
		add_filter( 'style_loader_src', 'rokjedna_remove_wp_ver_css_js', 9999 );
		// remove Wp version from scripts
		add_filter( 'script_loader_src', 'rokjedna_remove_wp_ver_css_js', 9999 );

	} // End of wpf_head_cleanup()
}


/**************************
    Cleaning functions
**************************/
/**
 * Remove WP version from RSS
 */
if( ! function_exists( 'rokjedna_rss_version ' ) ) {
	function rokjedna_rss_version() { return ''; }
}

/**
 * Remove WP version from scripts
 */
if( ! function_exists( 'rokjedna_remove_wp_ver_css_js ' ) ) {
	function rokjedna_remove_wp_ver_css_js( $src ) {

		// Array of files with exception
		$allowed = array("themes/rokjedna");

		foreach ( $allowed as $exception ) {
			if ( strpos($src,$exception) === false ) {

				if ( strpos( $src, 'ver=' ) ) {
					$src = remove_query_arg( 'ver', $src );
				}

			}
		}

		return $src;

	    
	}
}

/**
 * Remove injected CSS for recent comments widget
 */
if( ! function_exists( 'rokjedna_remove_wp_widget_recent_comments_style ' ) ) {
	function rokjedna_remove_wp_widget_recent_comments_style() {
	   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
	      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
	   }
	}
}

/**
 * Remove injected CSS from recent comments widget
 */
if( ! function_exists( 'rokjedna_remove_recent_comments_style ' ) ) {
	function rokjedna_remove_recent_comments_style() {
	  global $wp_widget_factory;
	  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
	    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
	  }
	}
}

?>
