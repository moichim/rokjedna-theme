<?php

/**
 * Pomůcky pro vývoj
 * 
 * @package jet
 * @author Jan Jáchim, jachim5@gmail.com
 */

/**
 * Vypisování kódu na frontendu
 * Pokud je zapnutý WP_DEBUG, vypíše $input rovnou v místě, kde je voláno.
 * 
 * @param any $input - cokoliv k vytisknutí
 * @param string $color - barva debugu
 */
if ( !function_exists("dev")) {
    function dev($input = NULL,$color = NULL) {
        if ( isset($input) && is_debug() ) {
            $background = empty($color) ? "gray" : $color;
            print "<pre class='verbosed' style='background-color:".$background."'>";
            print_r( $input );
            print "</pre>";
        }
    }
}



/**
 * Kontrola, jestli je zapnutý WP_DEBUG
 *
 * @return boolean
 */
if ( !function_exists("is_debug") ) {
    function is_debug() {
        $is = false;
        if (defined("WP_DEBUG")) {
            if (WP_DEBUG == true) {
                $is = true;
            }
        }
        return $is;
    }
}


/** 
 * Zjistí jméno aktuálně použité šablony
 */
if ( !function_exists("get_template_name") ) {
    function get_template_name() {
        global $template;
        return basename($template);
    }
}

$patterns = array(
    "page.php" => array(
        "0px" => "small.jpg",
        "640px" => "medium.jpg",
        "1024px" => "large.jpg",
    ),
);

/**
 * Vytiskne debugovací pozadí a styly na pozadí stránky
 */
if ( !function_exists( "frontend_pattern" ) ) {
    function frontend_pattern() {
        if ( is_debug() ) {
            // zjistí jméno šablony
            $template = get_template_name();
            // načte patřičnou šablonu
            global $patterns;
            if ( array_key_exists($template,$patterns) ) {
                $images = $patterns[$template];
                set_query_var("frontend",$images); 
                print "<style id='frontend_pattern'>";
                get_template_part("template-parts/debug/frontend_pattern");
                print "</style>";
            }
        }
    }
}

if ( is_debug() ) {
    function debug_body_class($classes) {
        $classes[] = 'frontend-pattern';
        return $classes;
    }
    
    add_filter('body_class', 'debug_body_class');
}