<?php

/**
 * Extras for the page post type
 * 
 * Is used for outputting the homapege calendar.
 * 
 * @package Rokjedna
 */


/**
 * CUSTOM FIELDS
 */
function rj_page_metabox() {

  $cmb = new_cmb2_box( array(
		'id'            => 'page_properties',
		'title'         => esc_html__( 'Rok Jedna', 'rokjedna' ),
		'object_types'  => array( 'page' ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		'context'    => 'side',
		// 'priority'   => 'high',
		'show_names' => false, // Show field names on the left
		'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		// 'classes'    => 'extra-class', // Extra cmb2-wrap classes
		// 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
  ) );
  
  $cmb->add_field( array(
		'name'         => translate('Main color of the section','rokjedna'),
		'id'           => 'section_color',
		'type'         => 'colorpicker',
		'description'	 => translate('Main color of the section. Fill in top level pages only!','rokjedna'),
		'attributes' => array(
			'data-colorpicker' => json_encode( array(
				// Iris Options set here as values in the 'data-colorpicker' array
				'palettes' => array(
					'#7B3DBA',
					'#3797B3',
					'#C73C81',
					'#0E53A7',
					'#C28C00',
					'#93A112',
					'#EC6317',
					'#7B3DBA',
					'#E3052E',
					'#0D9867',
					'#fff',
					'#F0F0F0',
					'#505050',
					'#181818',
					'#000'
				),
			) ),
		),
  ) );
  
  $cmb->add_field( array(
		'name'         => translate('Secondary color of the section','rokjedna'),
		'id'           => 'section_color_text',
		'type'         => 'colorpicker',
		'description'	 => translate("Secondary color of the section","rokjedna"),
		'attributes' => array(
			'data-colorpicker' => json_encode( array(
				// Iris Options set here as values in the 'data-colorpicker' array
				'palettes' => array(
					'#7B3DBA',
					'#3797B3',
					'#C73C81',
					'#0E53A7',
					'#C28C00',
					'#93A112',
					'#EC6317',
					'#7B3DBA',
					'#E3052E',
					'#0D9867',
					'#fff',
					'#F0F0F0',
					'#505050',
					'#181818',
					'#000'
				),
			) ),
		),
	) );

	$cmb->add_field( array(
		'name'    => translate('Paddings and boundaries','rokjedna'),
		'desc'    => translate('Paddings and boundaries.','rokjedna'),
		'id'      => 'post_paddings',
		'type'    => 'multicheck',
		'options' => array(
			'horizontal' => translate('Horizontal padding','rokjedna'),
			'vertical' => translate('Vertical padding','rokjedna'),
			'gutenberg' => 'Gutenberg editor',
		),
		'default' => array("gutenberg"),
	) );


}

add_action( 'cmb2_admin_init', 'rj_page_metabox' );

/**
 * TEMPLATE TAGS
 */

/**
 * Output the page section attributes - based on the page hierarchy
 */
if ( ! function_exists("rj_page_section_color") ) :

	function rj_page_section_color($post=NULL){

		// Load the current post if no parameter provided
		if ($post == NULL) { 
			$post = get_post(); 
		}

		// Load the post object if an integer is provided
		if ( gettype($post) == "integer" ) {
			$post = get_post($post);
		}

		// If the post is loaded properly, proceed
		if ( gettype($post) == "object" && $post != NULL ) {

			if ( get_post_type($post) == "page" ) {

				if ( $color = get_page_color($post) ) {

					return $color;

				} else {

					while ( $post->post_parent != 0 ) {

						$post = get_post( $post->post_parent );

						if ( $color = get_page_color($post) ) {
							return $color;
						}

					}

				}	

			}
		
		} 

	}

	function rj_page_section_title($post=NULL){

		// Load the current post if no parameter provided
		if ($post == NULL) { 
			$post = get_post(); 
		}

		// Load the post object if an integer is provided
		if ( gettype($post) == "integer" ) {
			$post = get_post($post);
		}

		// If the post is loaded properly, proceed
		if ( gettype($post) == "object" && $post != NULL ) {

			if ( get_post_type($post) == "page" ) {

				if ( $color = get_page_color($post) ) {

					return $post->post_title;

				} else {

					while ( $post->post_parent != 0 ) {

						$post = get_post( $post->post_parent );

						if ( $color = get_page_color($post) ) {
							return $post->post_title;
						}

					}

				}	

			}
		
		} 

	}

	function rj_page_section_color_text($post=NULL){

		// Load the current post if no parameter provided
		if ($post == NULL) { 
			$post = get_post(); 
		}

		// Load the post object if an integer is provided
		if ( gettype($post) == "integer" ) {
			$post = get_post($post);
		}

		// If the post is loaded properly, proceed
		if ( gettype($post) == "object" && $post != NULL ) {

			if ( get_post_type($post) == "page" ) {

				if ( $color = get_page_color_text($post) ) {

					return $color;

				} else {

					while ( $post->post_parent != 0 ) {

						$post = get_post( $post->post_parent );

						if ( $color = get_page_color_text($post) ) {
							return $color;
						}

					}

				}	

			}
		
		} 

	}

	function rj_page_section_slug($post=NULL){

		// Load the current post if no parameter provided
		if ($post == NULL) { 
			$post = get_post(); 
		}

		// Load the post object if an integer is provided
		if ( gettype($post) == "integer" ) {
			$post = get_post($post);
		}

		// If the post is loaded properly, proceed
		if ( gettype($post) == "object" && $post != NULL ) {

			if ( get_post_type($post) == "page" ) {

				if ( $color = get_page_color($post) ) {

					return $post->post_name;

				} else {

					while ( $post->post_parent != 0 ) {

						$post = get_post( $post->post_parent );

						if ( $color = get_page_color($post) ) {
							return $post->post_name;
						}

					}

				}	

			}
		
		}

	}

	function get_page_color($post){

		return get_post_meta($post->ID, "section_color", true);

	}

	function get_page_color_text($post){

		return get_post_meta($post->ID, "section_color_text", true);

	}

	function rj_page_section_thumbnail($post=NULL){

		// Load the current post if no parameter provided
		if ($post == NULL) { 
			$post = get_post(); 
		}

		// Load the post object if an integer is provided
		if ( gettype($post) == "integer" ) {
			$post = get_post($post);
		}

		// If the post is loaded properly, proceed
		if ( gettype($post) == "object" && $post != NULL ) {

			if ( get_post_type($post) == "page" ) {

				if ( $img = get_the_post_thumbnail_url($post) ) {

					return $img;

				} else {

					while ( $post->post_parent != 0 ) {

						$post = get_post( $post->post_parent );

						if ( $img = get_the_post_thumbnail_url($post) ) {
							return $img;
						}

					}

				}	

			}
		
		} 

	}


endif;

class CarouselManager {
	private $ready;
	private $count;
	private $posts;
	private $args;
	private $query;
	
	

	function __construct(){

		$this->ready = false;
		$this->posts = array();
		$this->count = 0;

		$this->args = array(
			"post_type" => "post",
			"posts_per_page" => 3,
			"orderby" => "date",
			"order" => "DESC",
			// 'post__in' => get_option( 'sticky_posts' ),
			"meta_query" => array(
				array(
					"key" => "post_carousels",
					'value' => serialize("front"),
					'compare' => 'LIKE',
				)
			),
		);
		$this->query = new WP_Query( $this->args );

		if ( $this->query->have_posts() ) {

			$this->ready = true;
			$this->count = count($this->query->posts);

			while ( $this->query->have_posts() ) {

				$this->query->the_post();

				array_push( $this->posts, get_post() );

			}
		}

		wp_reset_postdata();
	}

	public function render() {

		if ( $this->ready ) {
			$this->render_dots();
		}

	}

	private function get_slide_background( $post ) {
		$id = get_post_thumbnail_id($post);
		if ( $id ) {
			return "style='background-image:url(".wp_get_attachment_image_src($id,"full")[0].")'";
		}
	}

	public function render_dots() {

		foreach ( $this->posts as $key => $post ) {
			?>
				<li data-target="#carouselExampleIndicators" data-slide-to="<?= $key; ?>"<?php if ($key==0) :?> class="active"<?php endif; ?>></li>
			<?php
		}

	}

	public function render_slides() {
		foreach ( $this->posts as $key => $post ) {
			?>
				<article class="carousel-item rj-carousel-item rj-carousel-item_front <?php if ($key==0) :?> active<?php endif; ?>" <?= $this->get_slide_background($post); ?>>
					
					<div class="rj-carousel-item-text">
						<h2 class="rj-carousel-item-title">
							<a href="<?= get_post_permalink($post);?>"><?= get_the_title($post); ?></a><!-- TODO: switch to a once ready-->
						</h2>
						<div class="rj-carousel-item-excerpt"><?= get_the_excerpt($post); ?></div>
					</div>
				</article>
			<?php
		}
	}
}


if ( !function_exists("rj_home_carousel") ) {
	function rj_home_carousel(){
		$car = new CarouselManager();
		$car->render();
	}
}