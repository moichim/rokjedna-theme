<?php 

  $d = get_query_var("rj-contact");

  $counter = 0;

?>

<section class="rj-contacts">

  <ul class="nav" id="contactsTab" role="tablist">
  <?php foreach ( $d->output as $term ) :?>

    <?php 
      
      $classes = "";

      if ( $counter == 0 ) {

        $classes = "active";

      }
      
    ?>

    <li class="nav-item  ">
      <a 
        id="<?= $term["slug"]; ?>_tab" 
        href="#<?= $term["slug"]; ?>"
        class="nav-link <?= $classes; ?>" 
        role="tab"
        aria-controls="<?= $term["slug"]; ?>"
        data-toggle="<?= $term["slug"]; ?>"
      ><?= $term["name"]; ?></a>
    </li>
  <?php 
    $counter++;
    endforeach; 

    $counter = 0;
  ?>
  </ul>

  <div class="tab-content">
  <?php foreach ( $d->output as $term ) :?>

    <?php 
      
      $classes = "";

      if ( $counter == 0 ) {

        $classes = "show active";

      }
      
    ?>

    <div class="tab-pane <?= $classes; ?>" id="<?= $term["slug"]; ?>" role="tabpane" aria-labelledby="<?= $term["slug"]; ?>_tab" >

      <?php // $d->dev( $term ); ?>

      <div class="rj-contacts--list">

      <?php foreach ( $term["content"] as $person ) { ?>

        <article class="rj-contacts--person">

          <header class="rj-contacts--part">

            <div class="rj-contacts--image" style="background-image:url(<?= $person->contact_image; ?>)"></div>

            <h4 class="rj-contacts--name"><?= $person->post_title; ?></h4>

            <p><?= $person->contact_position; ?></p>

          </header>

          <div class="rj-contacts--part rj-contacts--part__stretcher">
            <p class="rj-contacts--link rj-contacts--mail">
              <img class="rj-contacts--icon" src="<?= rokjedna_image_url("icons/Obalka.svg");?>" alt="<?= translate("Obrázek obálky","rokjedna") ?>">
              <a href="mailto:<?= $person->contact_email ?>"><?= $person->contact_email ?></a>
            </p>

            <p class="rj-contacts--link rj-contacts--tel">
              <img class="rj-contacts--icon" src="<?= rokjedna_image_url("icons/Telefon.svg");?>" alt="<?= translate("Obrázek telefonu","rokjedna") ?>">
              <a href="tel:<?= $person->contact_telephone ?>"><?= $person->contact_telephone ?></a>
            </p>
          </div>

        </article>

      <?php }; ?>

      </div>


    
    </div>


  <?php 

    $counter++;

    endforeach; 
    
  ?>
  </div>

</section>
