import "jquery";

import "../../node_modules/slick-carousel/slick/slick";




$(document).ready(function(){

  let menu = $(".rj-menu_page");

  window.slides = $(".rj-menu_page .menu-item");

  // get the width of items
  window.itemsWidth = 0;
  window.slides.each(function(){

    window.itemsWidth += $(this).outerWidth();

  });

  checkSlider();

  $(window).resize(function(){
    
    // checkSlider();

  });


  function checkSlider(){

    // get width of the cintainer
    let contWidth = $( $(".rj-menu_page")[0] ).width();

      if ( window.slickActive ) {
        if (contWidth > window.itemsWidth ) {
          removeSlick();
        }
      }
      else {
        if (contWidth < window.itemsWidth ) {
          addSlick();
        }
      }

  }
  

  

  function addSlick() {

    console.log("přidávám slick");

    menu.on("init", function(){
      menu.addClass("rj-slick_activated");
    });
    menu.on("destroy", function(){
      menu.removeClass("rj-slick_activated");
    });

    // Calculate the width
    let w = window.innerWidth;
    let sum = 2;
    if ( window.slides.length % 2 === 0 ) {

      if (w>350) { sum=2; }
      if (w>700) { sum=4; }
      if (w>1000) { sum=4; }
      if (w>1200) { sum=6; }
      if (w>1400) { sum=8; }

    } else {

      if (w>350) { sum=3; }
      if (w>700) { sum=3; }
      if (w>1000) { sum=5; }
      if (w>1200) { sum=7; }

    }
    

    menu.slick({
      slides: window.slides,
      //centerMode: true,
      // infinite: true,
      arrows: true,
      slidesToShow: sum,
      prevArrow: '<button type="button" class="slick-prev rj-slider-arrow rj-slider-arrow_left" aria-label="Previous"></button>',
      nextArrow: '<button type="button" class="slick-next rj-slider-arrow rj-slider-arrow_right" aria-label="Next"></button>',
    });

  }

  function removeSlick(){
    window.slickActive = false;
    console.log("odstraňuji slick");
    menu.slick("unslick");
  }

  
});
