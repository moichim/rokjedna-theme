<?php
/**
 * Nastavení pro příspěvky
 * 
 * 
 */

function rj_post_metabox() {

  $cmb = new_cmb2_box( array(
		'id'            => 'post_properties',
		'title'         => esc_html__( 'Carousel', 'rokjedna' ),
		'object_types'  => array( 'post' ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		'context'    => 'side',
		// 'priority'   => 'high',
		'show_names' => false, // Show field names on the left
		'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		// 'classes'    => 'extra-class', // Extra cmb2-wrap classes
		// 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
  ) );

	$cmb->add_field( array(
		'name'    => translate('Push the post into carousels','rokjedna'),
		'desc'    => translate('Push the post into carousels','rokjedna'),
		'id'      => 'post_carousels',
		'type'    => 'multicheck',
		'options' => array(
			'front' => translate('Frontpage','rokjedna'),
			'archive' => translate('Archive','rokjedna'),
		),
	) );


}

add_action( 'cmb2_admin_init', 'rj_post_metabox' );


class ArchiveCarouselManager {
	private $ready;
	private $count;
	private $posts;
	private $args;
	private $query;

	function __construct(){

		$this->ready = false;
		$this->posts = array();
		$this->count = 0;

		$this->args = array(
			"post_type" => "post",
			"posts_per_page" => 3,
			"orderby" => "date",
			"order" => "DESC",
			"meta_query" => array(
				array(
					"key" => "post_carousels",
					'value' => serialize("archive"),
					'compare' => 'LIKE',
				)
			),
    );

		$this->query = new WP_Query( $this->args );

		if ( $this->query->have_posts() ) {

			$this->ready = true;
			$this->count = count($this->query->posts);

			while ( $this->query->have_posts() ) {

				$this->query->the_post();

				array_push( $this->posts, get_post() );

			}
		}

		wp_reset_postdata();

	}

	public function render() {

		if ( $this->ready ) {
			$this->render_dots();
		}

	}

	private function get_slide_background( $post ) {
		$id = get_post_thumbnail_id($post);
		if ( $id ) {
			return "style='background-image:url(".wp_get_attachment_image_src($id,"full")[0].")'";
		}
	}

	public function render_dots() {

		foreach ( $this->posts as $key => $post ) {
			?>
				<li data-target="#carouselBlogIndicators" data-slide-to="<?= $key; ?>"<?php if ($key==0) :?> class="active"<?php endif; ?>></li>
			<?php
		}

	}

	public function render_slides() {
		foreach ( $this->posts as $key => $post ) {
			?>
				<article class="carousel-item rj-carousel-item rj-carousel-item_archive <?php if ($key==0) :?> active<?php endif; ?>" <?= $this->get_slide_background($post); ?>>
					
					<div class="rj-carousel-item-text_archive">
          <div class="rj-post-detail-meta">
				    <?php rj_post_categories( $post->ID ); ?>
				    <span class="rj-dot">&bull;</span>
				    <?php rokjedna_posted_on( $post ); ?>
				    <span class="rj-dot">&bull;</span>
				    <?php rokjedna_posted_by( $post ); ?>
			    </div>
						<h2 class="rj-carousel-item-title">
							<a href="<?= get_post_permalink($post);?>"><?= get_the_title($post); ?></a><!-- TODO: switch to a once ready-->
            </h2>
            <div class="rj-carousel-readmore">
              <strong><?= __("Read more","rokjedna");?></strong>
            </div>
					</div>
				</article>
			<?php
		}
	}
}


if ( !function_exists("rj_archive_carousel") ) {
	function rj_archive_carousel(){
		$car = new ArchiveCarouselManager();
		$car->render();
	}
}

if ( !function_exists("rj_archive_list_categories") ) {

	function rj_archive_list_categories(){

		$cats = get_categories();

		print "<div class='rj-cats-list'>";

		foreach ( $cats as $cat ) {

			print "<a href='".get_term_link($cat)."' class='btn btn-dark btn-sm'>";
			print $cat->name;
			print "</a>";

		}


		print "</div>";

	}

}