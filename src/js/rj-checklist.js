import "jquery";

$(document).ready(function(){
    const links = $(".nav-link");
    const linkActiveClass = "active";
    const tabActiveClass = "active";
    const tabVisibleClass = "show";

    links.on("click",function( e ){

        e.preventDefault();

        const current = $(this);

        links.each(function(){
            if ( current.attr("id") !== $(this).attr("id") ) {
                if (  $(this).hasClass( linkActiveClass ) ) {
                    checkboxSetState( $(this), false );
                }
            }
            if ( current.attr("id") === $(this).attr("id")  ) {
                checkboxSetState( $(this), true );
            }
        });
    });

    /**
     * 
     * @param {jQuery object} link jQuery the master object.
     * @param {boolean} state Indicates whether turning it on and off.
     */
    function checkboxSetState( link, state ) {
        console.log( link );

        const tab = $( link.attr("href") );

        if ( state === true ) {
            link.addClass( linkActiveClass );
            tab.addClass( tabActiveClass ).addClass( tabVisibleClass );
        }
        if ( state === false ) {
            link.removeClass( linkActiveClass );
            tab.removeClass( tabActiveClass ).removeClass( tabVisibleClass );
        }
    }

});