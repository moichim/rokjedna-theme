<?php
/**
 * Define menus and its serving functions
 * 
 * @package Rokjedna
 */

/**
  * MENU DEFINITIONS
  */
if ( !function_exists( "rokjedna_menu_def" ) ) {

    function rokjedna_menu_def(){
  
      register_nav_menus(
              array(
                  'main-menu' => esc_html__( 'Main menu', 'rokjedna' ),
                  'global-menu' => esc_html__( 'Global menu', 'rokjedna'),
                  'footer-menu-left' => esc_html__( 'Footer menu left', 'rokjedna' ),
									'footer-menu-right' => esc_html__( 'Footer menu right', 'rokjedna' ),
									'social-menu' => esc_html__( 'Social networks in the footer', 'rokjedna' ),
              )
      );
      
    }
  
}
add_action( 'after_setup_theme', 'rokjedna_menu_def' );

/**
 * MENU TEMPLATE TAGS
 */

if ( ! function_exists('rj_page_menu') ):

	function rj_page_menu( $menu ) {

		?>
		<!-- The site main menu -->
		<nav id="rjPageMenu" class="rj-post-navigation" role="navigation" aria-labelledby="rj-m-page">

			<p id="rj-m-page" class="rj-vhide">
				<?= translate("Navigation within the section","rokjedna"); ?>
				<?= the_title(); ?>
			</p>

		<?php

		$menu_items = wp_get_nav_menu_items( $menu );
		$this_item = current( wp_filter_object_list( $menu_items, array( 'object_id' => get_queried_object_id() ) ) );

		$args = array(
			'menu_class' => "rj-menu rj-menu_page",
			'container' => false,
			'depth' => 2,
			'walker' => new Selective_Walker(),
			'theme_location' => $menu,
		);

		wp_nav_menu( $args );

		?>
		</nav>
		<?php

	}

endif;


if ( ! function_exists('rj_footer_menu') ):

	function rj_footer_menu( $menu ) {

		$args = array(
			'menu_class' => "rj-menu rj-menu_footer",
			'container' => "nav",
			'container_class' => "rj-f-column rj-f-column_widget",
			'depth' => 1,
			'theme_location' => $menu,
		);

		wp_nav_menu( $args );

	}

endif;

if ( ! function_exists('rj_global_menu') ):

	function rj_global_menu( $menu ) {

		$args = array(
			'menu_class' => "rj-menu rj-menu_global rj-menu_inline",
			'container' => "ul",
			'container_class' => "rj-f-column rj-f-column_widget",
			'depth' => 1,
			'theme_location' => $menu,
			'fallback_cb' => false
		);

		wp_nav_menu( $args );

	}

endif;

if ( ! function_exists('rj_main_menu') ):

	function rj_main_menu( $menu ) {

		?>
		<!-- The site main menu -->
		<nav id="rjMenu" class="rj-h-part rj-h-part_bottom rj-h-part_main-menu" role="navigation" aria-labelledby="rj-m-main">

			<p id="rj-m-main" class="rj-vhide"><?= esc_html_e("Main menu", "rokjedna"); ?></p>

		<?php

		// Print the menu itself
		$args = array(
			'menu_class' => "rj-menu rj-menu_main",
			'container' => false,
			'depth' => 1,
			'walker' => new Walker_Main_Menu(),
			'theme_location' => $menu,
			'fallback_cb' => false
		);

		wp_nav_menu( $args );

		?>
		<a id="rjMenuCloser" href="#rjAccessibilityMenu" aria-labelledby="rjMenuCloserLabel" class="rj-menu_main-close">
			<span id="rjMenuCloserLabel" class="rj-vhide"><?= translate("Close the menu","rokjedna"); ?></span>
			<span class="dashicons dashicons-no-alt"></span>
			<!-- <?= translate("Close the menu","rokjedna"); ?> -->
		</a>
		</nav>
		
		<?php

	}

endif;

if ( ! function_exists('rj_social_menu') ):

	function rj_social_menu( $menu ) {

		?>
		<!-- The site social menu -->
		<nav id="rjFooterMenu" class="" role="navigation" aria-labelledby="rj-m-social">

			<p id="rj-m-social" class="rj-vhide"><?= esc_html_e("Social network links", "rokjedna"); ?></p>

		<?php

		// Print the menu itself
		$args = array(
			'menu_class' => "rj-menu rj-menu_inline rj-menu_social",
			'container' => "nav",
			'depth' => 1,
			// 'walker' => new Walker_Main_Menu(),
			'theme_location' => $menu,
			'fallback_cb' => false
		);

		wp_nav_menu( $args );

		?>

		</nav>
		
		<?php

	}

endif;


if ( ! function_exists('rj_accessible_menu') ):

	function rj_accessible_menu( $menu ) {

		$args = array(
			'menu_class' => "rj-menu rj-menu_header",
			'container' => "nav",
			'container_class' => "rj-h-nav",
			'depth' => 0,
			'walker' => new Walker_Main_Menu(),
			'theme_location' => $menu,
			'fallback_cb' => false
		);

		wp_nav_menu( $args );

	}

endif;


/**
 * The breadcrumb is based on menu
 */

function wpd_get_menu_item( $field, $object_id, $items ){
	foreach( $items as $item ){
			if( $item->$field == $object_id ) return $item;
	}
	return false;
}


function wpd_nav_menu_breadcrumbs( ){

	// Get all menu locations
	$locations = get_nav_menu_locations();

	// get menu items by menu id, slug, name, or object
	$items = wp_get_nav_menu_items( $locations["main-menu"] );
	if( false === $items ){
			echo 'Menu not found';
			return;
	}

	// get the menu item for the current page
	$item = wpd_get_menu_item( 'object_id', get_queried_object_id(), $items );
	if( false === $item ){
			return;
	}

	// start an array of objects for the crumbs
	$menu_item_objects = array( $item );

	// loop over menu items to get the menu item parents
	while( 0 != $item->menu_item_parent ){
			$item = wpd_get_menu_item( 'ID', $item->menu_item_parent, $items );
			array_unshift( $menu_item_objects, $item );
	}

	// output crumbs
	$crumbs = array(); 
	$crumbs[0] = "<a href='".site_url('/')."'>".translate('Home','rokjedna')."</a>";
	foreach( $menu_item_objects as $menu_item ){

		// Exclude links to the self
		if ( $menu_item->object_id != get_the_ID() ) {
			$link = '<a href="%s">%s</a>';
			$crumbs[] = sprintf( $link, $menu_item->url, $menu_item->title );
		}
		
	}

	return count( $crumbs ) > 1 ? join( " <span>/</span>", $crumbs ) : false;


}