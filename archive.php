<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

get_header();

$c = new ArchiveCarouselManager();
?>

	<header class="page-header">
		<?php
			the_archive_title( '<h1 class="page-title rj-vhide">', '</h1>' );
			the_archive_description( '<div class="archive-description rj-vhide">', '</div>' );
		?>
	</header><!-- .page-header -->

	<section class="rj-blog-carousel rj-section_carousel">

		<div id="carouselBlogIndicators" class="carousel slide rj-carousel" data-ride="carousel" data-interval="5000" data-pause="false">
			<ol class="carousel-indicators rj-carousel-indicators">
				<?php $c->render_dots(); ?>
			</ol>
			<div class="carousel-inner rj-carousel-inner">
				<?php $c->render_slides(); ?>
			</div>
		</div>

	</section>

	<section>

	<?php rj_archive_list_categories() ?>

	</section>

	<div class="rj-archive">
		<div class="rj-archive-container">

		<?php if ( have_posts() ) : ?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', "teaser" );

			endwhile;


		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		</div>
	</div>

	<?php 
		the_posts_navigation( [
			"screen_reader_text" => __("Read more","rokjedna") 
		]); 
	?>

<?php
get_footer();
