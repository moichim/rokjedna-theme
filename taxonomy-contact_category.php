<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

get_header();

$c = new ArchiveCarouselManager();
?>

	<header class="page-header">
		<?php
			the_archive_title( '<h1 class="page-title rj-vhide">', '</h1>' );
			the_archive_description( '<div class="archive-description rj-vhide">', '</div>' );
		?>
	</header><!-- .page-header -->


	<div class="rj-archive">
		<div class="rj-archive-container">

		<?php if ( have_posts() ) : ?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', "contact" );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		</div>
	</div><!-- #main -->

<?php
get_footer();
