import 'jquery';

$(document).ready(function($){

  const ititMenuButtons = $();
  const ititSearchActivator = $('#rjItitActivator');
  const ititSearchDeactivator = $('#rjItitDeactivator');
  const ititMenuContainer = $("#rjItitContainer");
  const ititActiveClass = "rj-h-itit_search";
  const ititPassiveClass = "rj-h-itit_title";

  /**
   * Check if the itit component is active
   */
  function ititIsOn(){

    if ( ititMenuContainer.hasClass( ititActiveClass ) ) {
      return true;
    } else {
      return false;
    }

  }

  /**
   * Sets the itit state accordingly
   * @param {boolean} $state 
   */
  function ititSetState( $state ){

    if ($state) {
      ititMenuContainer.removeClass( ititPassiveClass );
      ititMenuContainer.addClass( ititActiveClass );
    } else {
      ititMenuContainer.removeClass( ititActiveClass );
      ititMenuContainer.addClass( ititPassiveClass );
    }

  }

  ititSearchActivator.on("click",function(){
    // console.log(this);
    ititSetState( true );
  });

  ititSearchDeactivator.on("click",function(){
    // console.log(this);
    ititSetState( false );
  });

});

