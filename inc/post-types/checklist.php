<?php

/**
 * Custom post type Checklist
 * 
 * Is used for outputting the homapege calendar.
 * 
 * @package Rokjedna
 */

 /**
  * DEFINITION
  */
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Monthly checklists', 'Post Type General Name', 'rokjedna' ),
		'singular_name'         => _x( 'Monthly checklist', 'Post Type Singular Name', 'rokjedna' ),
		'menu_name'             => __( 'Checklists', 'rokjedna' ),
		'name_admin_bar'        => __( 'Checklists', 'rokjedna' ),
		'archives'              => __( 'Item Archives', 'rokjedna' ),
		'attributes'            => __( 'Item Attributes', 'rokjedna' ),
		'parent_item_colon'     => __( 'Parent Item:', 'rokjedna' ),
		'all_items'             => __( 'All Months', 'rokjedna' ),
		'add_new_item'          => __( 'Add New Month', 'rokjedna' ),
		'add_new'               => __( 'Add New', 'rokjedna' ),
		'new_item'              => __( 'New Month', 'rokjedna' ),
		'edit_item'             => __( 'Edit Month', 'rokjedna' ),
		'update_item'           => __( 'Update Month', 'rokjedna' ),
		'view_item'             => __( 'View Month', 'rokjedna' ),
		'view_items'            => __( 'View Monthly Checklists', 'rokjedna' ),
		'search_items'          => __( 'Search Checklists', 'rokjedna' ),
		'not_found'             => __( 'Not found', 'rokjedna' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'rokjedna' ),
		'featured_image'        => __( 'Featured Image', 'rokjedna' ),
		'set_featured_image'    => __( 'Set featured image', 'rokjedna' ),
		'remove_featured_image' => __( 'Remove featured image', 'rokjedna' ),
		'use_featured_image'    => __( 'Use as featured image', 'rokjedna' ),
		'insert_into_item'      => __( 'Insert into item', 'rokjedna' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'rokjedna' ),
		'items_list'            => __( 'Items list', 'rokjedna' ),
		'items_list_navigation' => __( 'Items list navigation', 'rokjedna' ),
		'filter_items_list'     => __( 'Filter items list', 'rokjedna' ),
	);
	$args = array(
		'label'                 => __( 'Mohthly checklist', 'rokjedna' ),
		'description'           => __( 'Monthly checklist', 'rokjedna' ),
		'labels'                => $labels,
		'supports'              => array("title"),
		// 'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 50,
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => false,
		'can_export'            => false,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'menu_icon'             => "dashicons-yes-alt"
	);
	register_post_type( 'checklist', $args );

}

add_action( 'init', 'custom_post_type', 0 );



/**
 * CUSTOM FUELDS
 */
function rj_register_checklist_metaboxes() {

  $cmb = new_cmb2_box( array(
		'id'            => 'month_properties',
		'title'         => esc_html__( 'Month Data', 'rokjedna' ),
		'object_types'  => array( 'checklist' ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'side',
		// 'priority'   => 'high',
		// 'show_names' => false, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		// 'classes'    => 'extra-class', // Extra cmb2-wrap classes
		// 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
  ) );

  $cmb->add_field( array(
	'name'    => 'Month Number',
	'desc'    => 'Toto číslo musí být unikátní pro každý měsíc.',
	// 'default' => 'standard value (optional)',
	'id'      => 'month_number',
	'type'    => 'text_small',
	"attributes" => array(
		"type" => "number",
		"min" => 1,
		"max" => 12,
		"required" => true
	),
	'column' => array(
		'position' => 2,
		'name'     => 'Pořadové číslo měsíce',
	),
	) );
  
  $group_field_id = $cmb->add_field( array(
	'id'          => 'checklists',
	'type'        => 'group',
	// 'description' => __( 'Generates reusable form entries', 'cmb2' ),
	// 'repeatable'  => false, // use false if you want non-repeatable group
	'options'     => array(
		'group_title'       => __( 'Item {#}', 'rokjedna' ), // since version 1.1.4, {#} gets replaced by row number
		'add_button'        => __( 'Add Another Item', 'rokjedna' ),
		'remove_button'     => __( 'Remove Item', 'rokjedna' ),
		'sortable'          => true,
		'closed'         => true, // true to have the groups closed by default
		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
	),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
		'name' => __('Item title','rokjedna'),
		'id'   => 'title',
		'type' => 'text',
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => 'Description',
		// 'description' => 'Write a short description for this entry',
		'id'   => 'text',
		'type' => 'wysiwyg',
		'textarea_rows' => 3,
		'options' => array(
			'media_buttons' => false, // show insert/upload button(s)
			'teeny' => true
		)
	) );


}

add_action( 'cmb2_admin_init', 'rj_register_checklist_metaboxes' );


/**
 * FRONTPAGE MANAGER
 */


class ChecklistManager {
	private $args;
	public $posts;
	private $ready;
	
	function __construct() {
		
		// Prepare the arguments for the query 
		$this->args = array(
			"post_type" => "checklist",
			"orderby" => "meta_value_num",
			"meta_key" => "month_number",
			"order" => "ASC",
			"posts_per_page" => -1,
			"post_status" => "publish"
		);

		// Set default values to properties
		$this->ready = false;
		$this->posts = array();

		// Load the posts
		$this->load();

	}

	private function load() {

		if ( !$this->ready ) {

			$query = new WP_Query( $this->args );

			$tmp = array(); // temporary buffer
			$active_month = 0;
			$date_month = (int) date('n');

			// If the query has posts, store it
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {

					$query->the_post();

					$post = get_post();

					// Check if the post is within the current mont number
					$post_month = (int) get_post_meta( $post->ID, "month_number", true );

					if ( $post_month == $date_month ) {
						$post->month_active = true;
						$active_month = $post->month_active;
					} else {
						$post->month_active = false;
					}

					// load the checkboxes eventually
					$raw = get_post_meta($post->ID,"checklists", true);

					$post->post_month = (int) $post_month;
					$post->checkboxes = $raw ? $raw : false;

					array_push($tmp,$post);

				}

				// Restore the original data after the post being loaded
				wp_reset_postdata();

				
				

				// Prepend the current and future
				foreach ( $tmp as $month ) {

					if ( (int) $month->post_month >= (int) $date_month ) {
						array_push($this->posts,$month);
					}

				}
				// Postpone the past
				foreach ( $tmp as $month ) {

					if ( (int) $month->post_month < (int) $date_month ) {
						array_push($this->posts,$month);
					}

				}


				$this->ready = true;
			} else {
				$this->ready = false;
			}

		}

	}

	public function render() {

		print "<nav class='rj-ch-nav' aria-labelledby='rjChLabel'>";
		print "<p id='rjChLabel' class='rj-vhide'>".translate("Switch between months","rokjedna")."</p>";
		print "<ul class='nav nav-pills'>";

		// Render the switchers first 
		if ( count($this->posts) > 0 ) {
			foreach ( $this->posts as $post ) {
				set_query_var( "month", $post );
				get_template_part("template-parts/home/checklist","switcher");
			}
		}

		print "</ul>";
		print "</nav>";

		print "<div class='tab-content' id='rjChecklistTabContent'>";
		// Render the content second
		if ( count($this->posts) > 0 ) {
			foreach ( $this->posts as $post ) {
				set_query_var( "month", $post );
				get_template_part("template-parts/home/checklist","body");
			}
		}
		print "</div>";
	}
}

function rj_checklists() {
	$ch = new ChecklistManager();
	$ch->render();
}