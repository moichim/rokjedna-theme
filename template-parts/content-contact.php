<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

?>

<article class="rj-contacts--person">

          <header class="rj-contacts--part">

            <div class="rj-contacts--image" style="background-image:url(<?= $person->contact_image; ?>)"></div>

            <h4 class="rj-contacts--name"><?= $person->post_title; ?></h4>

            <p><?= $person->contact_position; ?></p>

          </header>

          <div class="rj-contacts--part rj-contacts--part__stretcher">
            <p class="rj-contacts--link rj-contacts--mail">
              <img class="rj-contacts--icon" src="<?= rokjedna_image_url("icons/Obalka.svg");?>" alt="<?= translate("Obrázek obálky","rokjedna") ?>">
              <a href="mailto:<?= $person->contact_email ?>"><?= $person->contact_email ?></a>
            </p>

            <p class="rj-contacts--link rj-contacts--tel">
              <img class="rj-contacts--icon" src="<?= rokjedna_image_url("icons/Telefon.svg");?>" alt="<?= translate("Obrázek telefonu","rokjedna") ?>">
              <a href="tel:<?= $person->contact_telephone ?>"><?= $person->contact_telephone ?></a>
            </p>
          </div>

</article>