<?php
/**
 * Template for displaying the start of the calendar
 */
$data = get_query_var("month");
$active = false;
$selected = "false";
if ( $data->month_active ) {
    $active = " active";
    $selected = "true";
}

?>
<li id="toggler_wrapper_<?= sanitize_title($data->post_title); ?>" class="nav-item">
    <a id="toggler_<?= sanitize_title($data->post_title); ?>" href="#tab_<?= sanitize_title($data->post_title); ?>" data-toggle="tab" class="nav-link<?= $active; ?>" aria-selected="<?= $selected; ?>" >
        <?= $data->post_title; ?>
    </a>
</li>