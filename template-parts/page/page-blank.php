<?php
/**
 * Template for pages with no padding at all
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="rj-post-content rj-gutenberg">
		<?php the_content(); ?>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
