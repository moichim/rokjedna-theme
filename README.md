# Šabloba pro Rokjedna

Vlastnosti:

- Pomocí **TGMPA** si šablona vynucuje instalaci potřebných pluginů (`inc/tgmpa.php`)
- Frontendové CSS a JS je řešeno **webpackem 4**

    - jQuery je načítáno z Wordpressu a není součástí šablony (*webpack.externals*)
    - vše, co se používá je ve složce `assets/`
    - zdrojové soubory ke všemu jsou ve složce `src/`

- **Bootstrap 4.5**:

    - Pro minimalizaci konfliktů CSS tříd je Bootstrap používán sémanticky - pomocí ručního volání mixinů ve vlastních třídách `.rj-*`
    - Výběr použitých komponent je v `src/scss/rokjedna.scss`
    - Z bootstrapovského JS je použito jen něco (Carousel a Tab). Nepoužité komponenty nejsou v bundlu přítomny. Viz `src/js/vendor/bootstrap.js`.
  
- konvence **BEM** pro namespacování CSS (prefix `.rj-`)

## Vývoj

CSS a JS je nutné řešit webpackem:

Instalace knihoven
```bash
npm install
```
Spuštění vývojového prostředí - sleduje změny v souborech a průběžně kompiluje `src/` do `assets/`
```
npm run start
```
Produkční build - do `assets` zkompiluje minifikované assety.
```
npm run build
```

## Třídy pro gutenberga

### Roztažení přes celou šířku

Pokud blok v adminu nenabízí výchozí gutenbergovské atributy pro roztažení bloku na celou šířku, je možné toho dosáhnout vložením následujících tříd:

- `.alignwide` - rozšířená
- `.alignfull` - přes celou šířku

### Odsazení

Odsazení bloků je udělané ručně v CSS. Pokud je potřeba jej přepsat, k dispozici jsou tyto třídy:

`.{typ}-{scope}-{strana}`

- `{typ}`: padding / margin
- `{scope}`: base / half / third / quarter / section
- `{strana}`: top / right / bottom / left / horizontal / vertical

Velikosti rozměrů jsou k nalezení [v souboru s proměnnými](https://bitbucket.org/moichim/rokjedna-theme/src/master/src/scss/_variables.scss). 

Rozměr **section** je určen pro simulování základního šířkového omezení obsahu.

Rozměr **section-wide** je určen pro simulování `.alignwide`.