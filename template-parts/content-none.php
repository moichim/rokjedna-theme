<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

?>

<section class="no-results not-found">

	<div class="rj-post-content rj-post-content_c rj-post-content_ph rj-post-content_pv rj-post-content_search">
	<h1 class="rj-post-title"><?php esc_html_e( 'Nothing Found', 'rokjedna' ); ?></h1>
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'rokjedna' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'rokjedna' ); ?></p>
			<?php

		else :
			?>

			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'rokjedna' ); ?></p>

			<section class="rj-post-header rj-post-header_search rj-post-header_colored">
  			<?php get_search_form(); ?>
			</section>

			<?php

		endif;
		?>
	</div>
</section>
