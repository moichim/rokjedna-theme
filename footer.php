<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Rokjedna
 */

?>
	</main>

	<!-- Accesibility links -->
	<nav id="rjAccessibilityFooterMenu" class="rj-accessibility" aria-labelledby="rj-accessibility-footer">
		<p id="rj-accessibility-footer" class="rj-vhide"><?= translate("Wou have reached the end of the main content. Select what to do next from accessibility links below.","rokjedna"); ?></p>
		<a title="<?= translate("Skip back to the beginning of this page","rokjedna"); ?>" href="#page" ><?= translate("Skip back to the start of this page","rokjedna"); ?></a>
		<a id="rjGoToMenu" href="#rjMenu" ><?= translate("Skip back to the main menu","rokjedna"); ?></a>
		<a id="rjGoToMenu" href="#rjFooter" ><?= translate("Continue to the footer below","rokjedna"); ?></a>
	</nav>

	<footer id="rjFooter" class="rj-f">

		<div class="rj-f-row">

			<div class="rj-f-column rj-f-column_info">

				<h3><?= get_bloginfo("name"); ?></h3>
				<p>Studentské otazníky spravuje tým IPC ZČU</p>

			</div>

			<?php rj_footer_menu("footer-menu-left") ?> 
			<?php rj_footer_menu("footer-menu-right") ?> 

			<div class="rj-f-column rj-f-column_social">
				<?php rj_social_menu("social-menu"); ?>

				<img class="rj-h-logo" src="<?= rokjedna_image_url("brand/ipc-logo.svg");?>" alt="<?= translate("The logo of IPC WBU in Pilsen","rokjedna") ?>">

			</div>

			<div class="rj-f-column rj-f-column_colophon">

				<div class="rj-colophon">
					
					<div class="rj-colophon-item rj-colophon-item_left">
					
						<span class="rj-dotted-item">Informační a poradenské centrum ZČU</span>
						<span class="rj-dotted-item_dot">&bull;</span>
						<span class="rj-dotted-item">Univerzitní 20, 301 00 Plzeň</span>
						<span class="rj-dotted-item_dot">&bull;</span>
						<span class="rj-dotted-item">UI 213</span>
						<span class="rj-dotted-item_dot">&bull;</span>
						<a class="rj-dotted-item" href="tel:+420377631350">+420 377 631 350-9</a>
						<span class="rj-dotted-item_dot">&bull;</span>
						<a class="rj-dotted-item" href="mailto:ipcentr@rek.zcu.cz">ipcentr@rek.zcu.cz</a>

					</div>

					<div class="rj-colophon-item rj-colophon-item_right">
					
						<span>© 2020 IPC ZČU</span>

					</div>

				</div>

			</div>


		</div>
		
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
