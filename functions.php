<?php
/**
 * Rokjedna functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Rokjedna
 */



/**
 * 1. The Template version
 */
if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	// define( '_S_VERSION', '1.0.5' );
	define( '_S_VERSION', time() );
}



/**
 * 2. External dependencies
 */

/**
 * 2.1. Lazy Blocks are included right from the template
 */
define( 'MY_LZB_PATH', get_stylesheet_directory() . '/inc/lzb/' );
define( 'MY_LZB_URL', get_stylesheet_directory_uri() . '/inc/lzb/' );

require_once MY_LZB_PATH . 'lazy-blocks.php';

add_filter( 'lzb/plugin_url', 'my_lzb_url' );
function my_lzb_url( $url ) {
    return MY_LZB_URL;
}

/**
 * 2.2. Required plugins through TGMPA
 */
require_once('inc/tgmpa.php');



/**
 * 3. Navigation
 */
require_once('inc/navigation/menus.php');
require_once('inc/navigation/nav-walker.php');



/**
 * 4. Post type specifics
 */
require_once('inc/post-types/global.php'); 
require_once('inc/post-types/page.php'); 
require_once('inc/post-types/checklist.php');
require_once('inc/post-types/post.php'); 
require_once('inc/post-types/contacts.php'); 
require_once('inc/post-types/carousel.php'); 



/**
 * 5. Gutenberg stuff
 */
require_once('inc/blocks.php'); 



/**
 * 6. Various utilities
 */
require_once('inc/development.php'); 
require_once('inc/cleanup.php');
require_once('inc/enqueue.php');
require_once('inc/utilites.php'); 
require_once('inc/template-tags.php');
require_once('inc/template-functions.php');
require_once('inc/customiser.php');



/**
 * 7. The template setup
 */


if ( ! function_exists( 'rokjedna_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function rokjedna_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Rokjedna, use a find and replace
		 * to change 'rokjedna' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'rokjedna', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Add theme support for selective refresh for widgets.
		// add_theme_support( 'customize-selective-refresh-widgets' );

		// Gutenberg features
		add_theme_support('align-wide');

		// REsponsive embeds
		add_theme_support( 'responsive-embeds' );

	}
endif;
add_action( 'after_setup_theme', 'rokjedna_setup' );

