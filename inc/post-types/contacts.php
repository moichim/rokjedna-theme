<?php

// Register Custom Post Type
function rj_contact_definition() {

	$labels = array(
		'name'                  => _x( 'Contacts', 'Post Type General Name', 'rokjedna' ),
		'singular_name'         => _x( 'Contact', 'Post Type Singular Name', 'rokjedna' ),
		'menu_name'             => __( 'Contacts', 'rokjedna' ),
		'name_admin_bar'        => __( 'Contacts', 'rokjedna' ),
		'archives'              => __( 'Item Archives', 'rokjedna' ),
		'attributes'            => __( 'Item Attributes', 'rokjedna' ),
		'parent_item_colon'     => __( 'Parent Person:', 'rokjedna' ),
		'all_items'             => __( 'All People', 'rokjedna' ),
		'add_new_item'          => __( 'Add New Person', 'rokjedna' ),
		'add_new'               => __( 'Add Person', 'rokjedna' ),
		'new_item'              => __( 'New Person', 'rokjedna' ),
		'edit_item'             => __( 'Edit Person', 'rokjedna' ),
		'update_item'           => __( 'Update Person', 'rokjedna' ),
		'view_item'             => __( 'View Item', 'rokjedna' ),
		'view_items'            => __( 'View Items', 'rokjedna' ),
		'search_items'          => __( 'Search Item', 'rokjedna' ),
		'not_found'             => __( 'Not found', 'rokjedna' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'rokjedna' ),
		'featured_image'        => __( 'Portrait', 'rokjedna' ),
		'set_featured_image'    => __( 'Set featured image', 'rokjedna' ),
		'remove_featured_image' => __( 'Remove featured image', 'rokjedna' ),
		'use_featured_image'    => __( 'Use as featured image', 'rokjedna' ),
		'insert_into_item'      => __( 'Insert into item', 'rokjedna' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'rokjedna' ),
		'items_list'            => __( 'Items list', 'rokjedna' ),
		'items_list_navigation' => __( 'Items list navigation', 'rokjedna' ),
		'filter_items_list'     => __( 'Filter items list', 'rokjedna' ),
	);
	$args = array(
		'label'                 => __( 'Contact', 'rokjedna' ),
		'description'           => __( 'Crew members', 'rokjedna' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		// 'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'             => 'dashicons-universal-access-alt'
	);
	register_post_type( 'contact', $args );

}
add_action( 'init', 'rj_contact_definition', 0 );

/**
 * Register contact taxonomies
 */

// Register Custom Taxonomy
function rj_contact_categories() {

	$labels = array(
		'name'                       => _x( 'Groups', 'Taxonomy General Name', 'rokjedna' ),
		'singular_name'              => _x( 'Group', 'Taxonomy Singular Name', 'rokjedna' ),
		'menu_name'                  => __( 'Groups', 'rokjedna' ),
		'all_items'                  => __( 'All Groups', 'rokjedna' ),
		'parent_item'                => __( 'Parent Group', 'rokjedna' ),
		'parent_item_colon'          => __( 'Parent Group:', 'rokjedna' ),
		'new_item_name'              => __( 'New Group Name', 'rokjedna' ),
		'add_new_item'               => __( 'Add New Group', 'rokjedna' ),
		'edit_item'                  => __( 'Edit Group', 'rokjedna' ),
		'update_item'                => __( 'Update Group', 'rokjedna' ),
		'view_item'                  => __( 'View Group', 'rokjedna' ),
		'separate_items_with_commas' => __( 'Separate groups with commas', 'rokjedna' ),
		'add_or_remove_items'        => __( 'Add or remove groups', 'rokjedna' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'rokjedna' ),
		'popular_items'              => __( 'Popular Groups', 'rokjedna' ),
		'search_items'               => __( 'Search Groups', 'rokjedna' ),
		'not_found'                  => __( 'Not Found', 'rokjedna' ),
		'no_terms'                   => __( 'No items', 'rokjedna' ),
		'items_list'                 => __( 'Groups list', 'rokjedna' ),
		'items_list_navigation'      => __( 'Group list navigation', 'rokjedna' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'contact_category', array( 'contact' ), $args );

}
add_action( 'init', 'rj_contact_categories', 0 );


/**
 * Meta fields
 */
function rj_contact_metabox() {

  $cmb = new_cmb2_box( array(
		'id'            => 'contact_properties',
		'title'         => esc_html__( 'Details', 'rokjedna' ),
		'object_types'  => array( 'contact' ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		'context'    => 'normal',
		// 'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'cmb_styles' => true, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		// 'classes'    => 'extra-class', // Extra cmb2-wrap classes
		// 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.
  ) );
  
  $cmb->add_field( array(
    'name'    => __('Position','rokjedna'),
    // 'desc'    => 'field description (optional)',
    // 'default' => 'standard value (optional)',
    'id'      => 'contact_position',
    'type'    => 'text',
  ) );

  $cmb->add_field( array(
    'name'    => __('Telephone','rokjedna'),
    // 'desc'    => 'field description (optional)',
    // 'default' => 'standard value (optional)',
    'id'      => 'contact_telephone',
    'type'    => 'text',
  ) );

  $cmb->add_field( array(
    'name'    => __('Email','rokjedna'),
    // 'desc'    => 'field description (optional)',
    // 'default' => 'standard value (optional)',
    'id'      => 'contact_email',
    'type'    => 'text',
  ) );


}

add_action( 'cmb2_admin_init', 'rj_contact_metabox' );



/**
 * Output manager
 */
class ContactsManager {
  private $ready;
  private $terms;
  public $output;
	
	

	function __construct(){

		$this->ready = false;
    
    $this->output = array();

    $this->charge();

		
  }

  private function charge() {

    $this->terms = get_terms("contact_category",array(
      'meta_key' => 'tax_position',
      'orderby' => 'tax_position'
    ));

    if ( count($this->terms) > 0 ) {

      $this->ready = true;

      foreach ( $this->terms as $term ) {

        // $this->dev( $term );


        // Charge the posts
        $args = array(
          'post_type' => 'contact',
          'tax_query' => array(
              array(
                  'taxonomy' => 'contact_category',
                  'field'    => 'term_id',
                  'terms'    => $term->term_id,
              ),
          ),
        );
        $query = new WP_Query( $args );

        if ( $query->have_posts() ) {

          $data = array(
            "name" => $term->name,
            "count" => $term->count,
            "id" => $term->term_id,
						"content" => array(),
						"slug" => $term->slug,
          );

          while ( $query->have_posts() ) {

						$query->the_post();
						
						$post = get_post();
						// $post->contact_data = get_post_meta( $post->ID );

						$post->contact_position = get_post_meta( $post->ID,"contact_position", true );
						$post->contact_telephone = get_post_meta( $post->ID,"contact_telephone", true );
						$post->contact_email = get_post_meta( $post->ID,"contact_email", true );
						$post->contact_image = get_the_post_thumbnail_url( $post->ID, array(200,200) );

            array_push($data["content"],$post );


          }

          wp_reset_postdata();

					array_push( $this->output, $data );
					
					

        }

      }

		}

  }
  
  public function dev( $thing ) {
    print "<pre>";
    print_r( $thing );
    print "</pre>";
  }

	public function render() {
    // $this->dev( $this->output );

    set_query_var( "rj-contact", $this );

    get_template_part("template-parts/partials/contact-accordion");

	}

}

function contacts_all(){
  $c = new ContactsManager();
  $c->render();
}