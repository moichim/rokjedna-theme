<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class("rj-post-detail"); ?>>
	<header class="rj-post-detail-header">

		<?php if ( 'post' === get_post_type() ) : ?>
			<div class="rj-post-detail-meta">
				<?php rj_post_categories(); ?>
				<span class="rj-dot">&bull;</span>
				<?php rokjedna_posted_on(); ?>
				<span class="rj-dot">&bull;</span>
				<?php rokjedna_posted_by(); ?>
			</div>
		<?php endif; ?>

		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="rj-post-detail-title">', '</h1>' );
		else :
			the_title( '<h2 class="rj-post-detail-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		?>
	</header>

	<div class="rj-post-content rj-gutenberg  rj-post-content_pv rj-post-content_gutenberg">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'rokjedna' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

	wp_link_pages(
			array(
				'before' => '<div class="rj-page-links">' . esc_html__( 'Pages:', 'rokjedna' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php // rokjedna_entry_footer(); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->


<section class="rj-archive rj-archive_gray">

		<h2 class="rj-archive--title">Pokračujte ve čtení</h2>

		<div class="rj-archive-container">

		<?php
         
        $nextPost = get_next_post(true);
        if($nextPost) {
            $args = array(
                'posts_per_page' => 1,
                'include' => $nextPost->ID
            );
            $nextPost = get_posts($args);
            foreach ($nextPost as $post) {
                setup_postdata($post);
    ?>
        <div class="rj-archive-item" role="listitem" aria-atomic="true">
						<div class="rj-archive--hint"><?= __("Next post","rokjedna"); ?></div>
            <a class="rj-archive-item-wrap" href="<?php the_permalink(); ?>">

						<header>
						
							

							<?php the_post_thumbnail("thumbnail"); ?>

							<?php the_title( sprintf( '<h2 class="rj-post-list-item-title">', esc_url( get_permalink() ) ), '</h2>' ); ?>

							<?php if ( 'post' === get_post_type() ) : ?>
								<div class="entry-meta">
									<?php
									rokjedna_posted_on();
									?>
								</div><!-- .entry-meta -->
							<?php endif; ?>

							<?php if ( $slug && $title = rj_page_section_title() != get_the_title() ) : ?>
								<div class="rj-post-list-item-footer">
									<span class="rj-post-list-item-label">V sekci</span> <span class="rj-page-section-name"><?= rj_page_section_title(); ?><span>
								</div>
							<?php endif; ?>

						</header>

						<div>
							<?= get_the_excerpt(); ?>
						</div>

						<div class="rj-archive-detail-readmore"><?= __("Read more","rokjedna"); ?></div>

						</a>
        </div>
    <?php
                wp_reset_postdata();
            } //end foreach
        } // end if
    ?>



    <?php $prevPost = get_previous_post(true);
        if($prevPost) {
            $args = array(
                'posts_per_page' => 1,
                'include' => $prevPost->ID
            );
            $prevPost = get_posts($args);
            foreach ($prevPost as $post) {
                setup_postdata($post);
    ?>
        <div class="rj-archive-item" role="listitem" aria-atomic="true">
				<div class="rj-archive--hint"><?= __("Previous post","rokjedna"); ?></div>
            <a class="rj-archive-item-wrap" href="<?php the_permalink(); ?>">

						<header>
						
							

							<?php the_post_thumbnail("thumbnail"); ?>

							<?php the_title( sprintf( '<h2 class="rj-post-list-item-title">', esc_url( get_permalink() ) ), '</h2>' ); ?>

							<?php if ( 'post' === get_post_type() ) : ?>
								<div class="entry-meta">
									<?php
									rokjedna_posted_on();
									?>
								</div><!-- .entry-meta -->
							<?php endif; ?>

							<?php if ( $slug && $title = rj_page_section_title() != get_the_title() ) : ?>
								<div class="rj-post-list-item-footer">
									<span class="rj-post-list-item-label">V sekci</span> <span class="rj-page-section-name"><?= rj_page_section_title(); ?><span>
								</div>
							<?php endif; ?>

						</header>

						<div>
							<?= get_the_excerpt(); ?>
						</div>

						<div class="rj-archive-detail-readmore"><?= __("Read more","rokjedna"); ?></div>

						</a>
        </div>
    <?php
                wp_reset_postdata();
            } //end foreach
				} // end if
				
		?>

		</div>
</section>