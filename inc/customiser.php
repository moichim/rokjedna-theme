<?php
/**
 * Settings for the theme.
 *
 * @package Rokjedna
 * @since FoundationPress 1.0.0
 */


if ( ! function_exists( 'ir_register_theme_customizer' ) ) :
	function ir_register_theme_customizer( $wp_customize ) {

		// Create custom panels
		$wp_customize->add_panel(
			'rj_settings', array(
				'priority'       => 1000,
				'theme_supports' => '',
				'title'          => __( 'Rok Jedna', 'rokjedna' ),
				'description'    => __( 'The current theme options.', 'rokjedna' ),
			)
		);

		// Create custom field for mobile navigation layout
		$wp_customize->add_section(
			'rj_settings_section', array(
				'title'    => __( 'Site settings', 'rokjedna' ),
				'panel'    => 'rj_settings',
				'priority' => 1000,
			)
		);

		// Settings definitions

		$wp_customize->add_setting(
			'rj_blog_category',
			array(
				// 'default' => 'poptavka@pronajmy-epre.cz',
			)
		);

		// Add controls

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'rj_blog_category',
				array(
					'type'     => 'number',
					'section'  => 'rj_settings_section',
					'settings' => 'rj_blog_category',
					'label' => 'Vstupní kategorie blogu',
          'description' => 'Vložte ID kategorie, která slouží jako vstup do blogu. Tato kategori bude sloužit jako cíl odkazu "Studenti studentům".'
				)
			)
		);


	}

	add_action( 'customize_register', 'ir_register_theme_customizer' );

	// load pages of search page template

	function epp_admin_choices_page_search(){

		// load pages
		$args = array(
			"post_type" => array( "page" ),
			'meta_key' => '_wp_page_template',
      'meta_value' => 'page-templates/search.php'
		);
		$q = new WP_Query( $args );

		// Prepare the output
		$o = array();
		if ( $q->have_posts() ) {
			foreach($q->posts as $p){
				$o[ $p->ID ] = $p->post_title;
			}
		}

		// Return values
		if ( count( $o ) > 0 ) {
			return $o;
		} else {
			return false;
		}

	}

	function epp_search_page_id(){

		$sp = get_theme_mod( 'epp_search_page' );

		return $sp;

	}

	function epp_search_page_object(){

		$sp = epp_search_page_id();

		if ( $sp ) {

			return get_page( $sp );

		} else {

			return false;

		}

	}

	function epp_contact_field( $field ){
		return get_theme_mod( $field );
	}



endif;
