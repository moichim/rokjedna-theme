<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Rokjedna
 */

get_header();
?>

		<section class="error-404 not-found">

			<div class="rj-post-content rj-post-content_c rj-post-content_pv rj-post-content_ph rj-post-content_search">
				<h1 class="rj-post-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'rokjedna' ); ?></h1>
				<p><?php esc_html_e( 'It looks like nothing was found at this location. What about trying the search?', 'rokjedna' ); ?></p>

			</div><!-- .page-content -->
		</section><!-- .error-404 -->

		<section class="rj-post-header rj-post-header_search rj-post-header_colored">

			<?php get_search_form(); ?>

		</section>


<?php
get_footer();
