<article class="wp-block-homepage-desk rj-block rj-block_homepage-desk" id="<?= sanitize_title( block_value("title") ); ?>">
    <style>
        #<?= sanitize_title( block_value("title") ); ?> a:hover .rj-block_homepage-desk_text,
        #<?= sanitize_title( block_value("title") ); ?> a:focus .rj-block_homepage-desk_text {
            background-color: <?= block_value("color"); ?>
        }
    </style>
    <a class="rj-block_homepage-desk_link" href="<?= block_value("link"); ?>" title="Odkaz na sekci <?= block_value("title"); ?>">
        <?= wp_get_attachment_image( block_value("image"), 'medium', false, array("class"=> "rj-block_homepage-desk_image") ); ?>
        <header class="rj-block_homepage-desk_text">
            <h2><?= block_value("title"); ?></h2>
            <p><?= block_value("text");?></p>
        </header>
    </a>
</article>