<?php
/**
 * Template for displaying the start of the calendar
 */
$data = get_query_var("month");
$active = false;
if ( $data->month_active ) {
    $active = " show active";
}

?>

<div id="tab_<?= sanitize_title($data->post_title); ?>" class="rj-ch-tab tab-pane <?= $active; ?>" aria-labelledby="toggler_<?= sanitize_title($data->post_title); ?>" role="tabpanel">
   <ul class="rj-ch-list">
   <?php
        if ( $data->checkboxes ) {
            foreach ( $data->checkboxes as $checkbox) {
                echo "<li class='rj-ch-check'>";
                echo "<strong class='rj-ch-check-title'>".$checkbox["title"]."</strong>";
                echo "<div class='rj-ch-check-text'>" . wpautop($checkbox["text"]) ."</div>";
                echo "</li>";
            }
        }
   
   ?>
   </ul>
</div>