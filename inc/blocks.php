<?php

/**
 * LazyBlock blocks definitions
 */

if ( function_exists( 'lazyblocks' ) ) :

    lazyblocks()->add_block( array(
        'id' => 393,
        'title' => 'Dlaždice na homepage',
        'icon' => '<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
    <rect opacity="0.25" width="15" height="15" rx="4" transform="matrix(-1 0 0 1 22 7)" fill="currentColor" />
    <rect width="15" height="15" rx="4" transform="matrix(-1 0 0 1 17 2)" fill="currentColor" />
    </svg>
    ',
        'keywords' => array(
        ),
        'slug' => 'lazyblock/rj-desk',
        'description' => '',
        'category' => 'lazyblocks',
        'category_label' => 'lazyblocks',
        'supports' => array(
            'customClassName' => false,
            'anchor' => false,
            'align' => array(
                1 => 'full',
            ),
            'html' => false,
            'multiple' => true,
            'inserter' => true,
        ),
        'ghostkit' => array(
            'supports' => array(
                'spacings' => false,
                'display' => false,
                'scrollReveal' => false,
            ),
        ),
        'controls' => array(
            'control_6d2b2f4132' => array(
                'type' => 'text',
                'name' => 'title',
                'default' => '',
                'label' => 'Nadpis',
                'help' => '',
                'child_of' => '',
                'placement' => 'inspector',
                'width' => '100',
                'hide_if_not_selected' => 'false',
                'save_in_meta' => 'false',
                'save_in_meta_name' => '',
                'required' => 'false',
                'placeholder' => 'Vložte nadpis dlaždice',
                'characters_limit' => '',
            ),
            'control_c5baaa46cb' => array(
                'type' => 'textarea',
                'name' => 'text',
                'default' => '',
                'label' => 'Text dlaždice',
                'help' => '',
                'child_of' => '',
                'placement' => 'inspector',
                'width' => '100',
                'hide_if_not_selected' => 'false',
                'save_in_meta' => 'false',
                'save_in_meta_name' => '',
                'required' => 'false',
                'placeholder' => 'Vložte text dlaždice',
                'characters_limit' => '',
            ),
            'control_f5486f45fd' => array(
                'type' => 'image',
                'name' => 'image',
                'default' => '',
                'label' => 'Image',
                'help' => '',
                'child_of' => '',
                'placement' => 'inspector',
                'width' => '100',
                'hide_if_not_selected' => 'false',
                'save_in_meta' => 'false',
                'save_in_meta_name' => '',
                'required' => 'false',
                'placeholder' => '',
                'characters_limit' => '',
            ),
            'control_b9ba78480f' => array(
                'type' => 'url',
                'name' => 'url',
                'default' => '',
                'label' => 'URL',
                'help' => '',
                'child_of' => '',
                'placement' => 'inspector',
                'width' => '100',
                'hide_if_not_selected' => 'false',
                'save_in_meta' => 'false',
                'save_in_meta_name' => '',
                'required' => 'false',
                'placeholder' => '',
                'characters_limit' => '',
            ),
            'control_c4d9454140' => array(
                'type' => 'color',
                'name' => 'color',
                'default' => '',
                'label' => 'Barva',
                'help' => '',
                'child_of' => '',
                'placement' => 'inspector',
                'width' => '100',
                'hide_if_not_selected' => 'false',
                'save_in_meta' => 'false',
                'save_in_meta_name' => '',
                'required' => 'false',
                'placeholder' => '',
                'characters_limit' => '',
            ),
        ),
        'code' => array(
            'editor_html' => '<article class="rj-d">
        <img src="{{image.url}}" alt="{{image.alt}}">
            <header class="rj-d-header">
                <h2 class="rj-d-title">{{title}}</h2>
                <p class="rj-d-text">{{text}}</p>
            </header>
    </article>',
            'editor_callback' => '',
            'editor_css' => '',
            'frontend_html' => '<article class="rj-d rj-d_{{blockId}}">
        <style>
            .rj-d_{{ blockId }}:hover .rj-d-header,
            .rj-d_{{ blockId }} .rj-d-link:focus .rj-d-header {
                background-color: {{color}};
            }
        </style>
        <a class="rj-d-link" href="{{url}}">
            <img src="{{image.url}}" alt="{{image.alt}}">
            <header class="rj-d-header">
                <h2 class="rj-d-title">{{title}}</h2>
                <div class="rj-d-text">{{text}}</div>
            </header>
        </a>
    </article>',
            'frontend_callback' => '',
            'frontend_css' => '',
            'show_preview' => 'always',
            'single_output' => false,
            'use_php' => false,
        ),
        'condition' => array(
        ),
    ) );
    
endif;

/** 
 * Eliminate wrapping of rendered blocks
 */
add_filter( 'lazyblock/rj-desk/frontend_allow_wrapper', '__return_false' );