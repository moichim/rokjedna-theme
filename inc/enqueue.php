<?php

/**
 * Enqueue the scripts and styles.
 * 
 * @package Rokjedna
 */

function rokjedna_scripts() {
	wp_enqueue_style( 'rokjedna-css-main', get_stylesheet_uri(), array(), _S_VERSION );
	
	// wp_style_add_data( 'rokjedna-style', 'rtl', 'replace' );
	
	wp_enqueue_script( 'jquery' );

	wp_enqueue_style( 'dashicons' );

	wp_enqueue_style( 'rokjedna-css', get_template_directory_uri() . '/assets/rokjedna.css', array(), _S_VERSION );

	wp_enqueue_script( 'rokjedna-js', get_template_directory_uri() . '/assets/rokjedna.js', array('jquery'), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'rokjedna_scripts' );

/**
 * Registers an editor stylesheet for the theme.
 */
function rokjedna_editor_styles() {
	add_editor_style( trailingslashit( get_template_directory_uri() ) . 'assets/editor.css' );
	// wp_enqueue_style( 'rokjedna', get_theme_file_uri( '/assets/editor.css' ), true );
}
add_action( 'admin_init', 'rokjedna_editor_styles' );

add_action( 'enqueue_block_editor_assets', 'rokjedna_editor_styles' );