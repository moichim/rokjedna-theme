<?php
/**
 * Page template for search
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

?>


<section class="rj-post-header rj-post-header_search rj-post-header_colored">

<?php the_title( '<h1 class="rj-vhide">', '</h1>' ); ?>
  <?php get_search_form(); ?>

</section>

<article id="post-<?php the_ID(); ?>" <?php post_class("rj-page rj-page_section"); ?>>

	<div class="rj-post-content <?php rj_post_gutter_classes( get_post() ); ?>">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="rj-page-links">' . esc_html__( 'Pages:', 'rokjedna' ),
				'after'  => '</div>',
			)
		);
		?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
