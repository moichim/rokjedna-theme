<?php
/**
 * Page template formatter for pages with no wrapper
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class("rj-page rj-page_section"); ?>>
	<header class="rj-post-header rj-post-header_colored" style="background-image:url(<?= rj_page_section_thumbnail(); ?>)">
		<div class="rj-post-header--wrapper">
			<?php the_title( '<h1 class="rj-post-title">', '</h1>' ); ?>

			<?php 
				$breadcrumb = wpd_nav_menu_breadcrumbs();
				if ( $breadcrumb ): 
			?>
			<nav class="rj-post-breadcrumb" role="navigation" aria-labelledby="rjPageBreadcrumb">
				<p id="rjPageBreadcrumb" class="rj-vhide"><?= translate("Breadcrumb navigation - links to parent pages","rokjedna"); ?></p>
				<?= $breadcrumb; ?>
			</nav>
			<?php endif; ?>

		</div>
	</header>
	
	<?php rj_page_menu("main-menu"); ?>

	<div class="rj-post-content rj-gutenberg <?php rj_post_gutter_classes( get_post() ); ?>">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="rj-page-links">' . esc_html__( 'Pages:', 'rokjedna' ),
				'after'  => '</div>',
			)
		);
		?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
<section class="smrt">
<?php
  contacts_all();
?>
</section>