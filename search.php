<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Rokjedna
 */

get_header();
?>

	<section class="rj-post-header rj-post-header_search rj-post-header_colored">

		<?php get_search_form(); ?>

	</section>

	<section class="site-main rj-post-list" aria-labelledby="rjSearchDescription">

		<?php if ( have_posts() ) : ?>

			<h1 id="rjSearchDescription" class="rj-vhide">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'rokjedna' ), '<span>' . get_search_query() . '</span>' );
					?>
			</h1>

			<div class="rj-post-list-row rj-post-content rj-post-content_pv rj-post-content_ph rj-post-content_search" role="list">

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'template-parts/content', 'search' );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
		</div>

	</section>

<?php
get_footer();
