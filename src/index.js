/**
 * CSS
 */

// The main file for export
import './scss/rokjedna.scss';

// The editor style
import './scss/editor.scss';

/**
 * JS components embedded one by one
 */
import './js/rj-menu';
import './js/rj-slick';
import './js/rj-checklist';

import './js/vendor/bootstrap';

/**
 * Static images
 */
import './img/brand/logo.svg';
import './img/brand/ipc-logo.svg';
import './img/icons/pseudotodo.jpg';