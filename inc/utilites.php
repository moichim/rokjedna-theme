<?php
/**
 * Miscelannious utilities
 * 
 * @package Rokjedna
 */

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 */
function rokjedna_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'rokjedna_content_width', 1200 );
}
add_action( 'after_setup_theme', 'rokjedna_content_width', 0 );

/**
 * Post excerpt length
 */
function rokjedna_excerpt_length( $length ) {
  return 20;
}
add_filter( 'excerpt_length', 'rokjedna_excerpt_length', 999 );

/**
 * The Palette for Gutenberg
 */

function rokjedna_gutenberg_palette() {
  add_theme_support( 'editor-color-palette', array(
      array(
        'name' => __( 'Primární barva', 'rokjedna' ),
        'slug' => 'primary',
        'color' => '#7B3DBA',
      ),
      array(
        'name' => __( 'Studuj jako profík', 'rokjedna' ),
        'slug' => 'studuj-jako-profik',
        'color' => '#3797B3',
      ),
      array(
        'name' => __( 'Nech si poradit', 'rokjedna' ),
        'slug' => 'nech-si-poradit',
        'color' => '#C73C81',
    ),
      array(
          'name' => __( 'Poznej ZČU', 'rokjedna' ),
          'slug' => 'poznej-zcu',
          'color' => '#0E53A7',
      ),
      array(
          'name' => __( 'Objevuj Plzeň', 'rokjedna' ),
          'slug' => 'objevuj-plzen',
          'color' => '#C28C00',
      ),
      array(
          'name' => __( 'Buď aktivní', 'rokjedna' ),
          'slug' => 'bud-aktivni',
          'color' => '#93A112',
      ),
      array(
        'name' => __( 'Bez bariér', 'rokjedna' ),
        'slug' => 'bez-barier',
        'color' => '#EC6317',
      ),
      array(
        'name' => __( 'FAQ', 'rokjedna' ),
        'slug' => 'faq',
        'color' => '#7B3DBA',
      ),
      array(
        'name' => __( 'Blog', 'rokjedna' ),
        'slug' => 'blog',
        'color' => '#E3052E',
      ),
      array(
        'name' => __( 'Kontakt', 'rokjedna' ),
        'slug' => 'kontakt',
        'color' => '#0D9867',
      ),
      array(
        'name' => __( 'Bílá', 'rokjedna' ),
        'slug' => 'white',
        'color' => '#fff',
      ),
      // 
      array(
        'name' => __( 'Světlounce šedá', 'rokjedna' ),
        'slug' => 'gray-light',
        'color' => '#F0F0F0',
      ),
      array(
        'name' => __( 'Tmavě šedá', 'rokjedna' ),
        'slug' => 'gray-dark',
        'color' => '#505050',
      ),
      array(
        'name' => __( 'Pozadí patičky', 'rokjedna' ),
        'slug' => 'footer',
        'color' => '#181818',
      ),
      array(
        'name' => __( 'Černá', 'rokjedna' ),
        'slug' => 'black',
        'color' => '#000',
      ),
      
  ) );
}

add_action( 'after_setup_theme', 'rokjedna_gutenberg_palette' );

/**
 * The Palette for TinyMCE and other stuff
 */
add_action( 'customize_controls_enqueue_scripts', 'rokjedna_tinymce_palette' );

/**
 * Adds custom color palettes to wp.color picker.
 */
function rokjedna_tinymce_palette() {
	$color_palettes = json_encode(
		array(
			'#673ab7',
			'#3f51b5',
			'#2196f3',
			'#00bcd4',
			'#009688',
			'#4caf50',
		)
	);
	wp_add_inline_script( 'wp-color-picker', 'jQuery.wp.wpColorPicker.prototype.options.palettes = ' . $color_palettes . ';' );
}

