<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Rokjedna
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<!-- Přizpůsobení vzhledu stránky -->
	<?php 
		if ( is_page() ) {

			if ( $slug = rj_page_section_slug() ) {

				$main = rj_page_section_color();
				$text = rj_page_section_color_text();

				if ($main) {

					?>
					<style>

						.rj-post-header.rj-post-header_colored {
							background-color: <?= $main; ?>;
							position: relative;
						}

						.rj-post-header.rj-post-header_colored .rj-post-header--wrapper {
							z-index: 2;
							position: relative;
							width: 100%;
						}

						.rj-post-header.rj-post-header_colored:before {
							
							width: 100%;
							height: 100%;

							position: absolute;
							top: 0px;
							left: 0px;

							opacity: .5;
							content: "";
							background-color: <?= $main; ?>;

							z-index: 1;
						}

						.rj-menu_page a {
							color: <?= $main; ?>;
						}
						.rj-page-links > span,
						.rj-page-links > a:hover,
						.rj-page-links > a:focus {
							background-color: <?= $main; ?>;
						}
						.rj-page-links > *,
						.rj-slider-arrow_left,
						.rj-slider-arrow_right {
							color: <?= $main; ?>;
						}

						.rj-menu_page:not(.rj-slick_activated) .menu-item.current-menu-item,
						.rj-menu_page:not(.rj-slick_activated) .menu-item.current-menu-ancestor  {
							border-bottom-color: <?= $main; ?>;
						}

						.c-accordion__item .c-accordion__title:hover,
						.c-accordion__item .c-accordion__title:focus {
							background-color: <?= $main; ?>;
							color: white;
						}
						.c-accordion__item .c-accordion__title:hover:after,
						.c-accordion__item .c-accordion__title:focus:after {
							color: white;
						}

						.c-accordion__item .c-accordion__title {
							border-color: <?= $main; ?>;
							color: <?= $main; ?>;
						}

						.c-accordion__item .c-accordion__title:after {
							color: <?= $main; ?>;
						}

					</style>

					<?php

				}


			}

		}
	?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="rj-site">

	<!-- Accesibility links -->
	<nav id="rjAccessibilityMenu" class="rj-accessibility" aria-labelledby="rj-accessibility">
		<p id="rj-accessibility" class="rj-vhide"><?= translate("Accessible navigation","rokjedna"); ?></p>
		<a title="Skip to the main content (access key 2)" accesskey="2" href="#rjMain" tabindex="1"><?= translate("Skip to the main content","rokjedna"); ?></a>
		<?php if( !rj_is_blog() ): ?>
		<a id="rjGoToMenu" href="#rjMenu" tabindex="1"><?= translate("Skip to the main menu","rokjedna"); ?></a>
		<?php endif; ?>
		<?php if ( is_page() ) :?>
			<a id="rjGoToMenu" href="#rjPageMenu" tabindex="1"><?= translate("Go to the list of pages contained in this section ","rokjedna"); ?></a>
		<?php endif; ?>
	</nav>

	<!-- The site header -->
	<header id="rjItitContainer" class="rj-h rj-h-itit_title">

		<!-- Site branding -->
		<a href="<?= get_bloginfo("home"); ?>" class="rj-h-part rj-h-part_top rj-h-part_branding" tabindex="-1">
			<img class="rj-h-logo" src="<?= rokjedna_image_url("brand/logo.svg");?>" alt="<?= translate("The logo of WBU in Pilsen","rokjedna") ?>">
			<?php if ( rj_is_blog() ): ?>
				<span class="rj-h-logo_blog"><?php bloginfo("name"); ?></span>
			<?php endif; ?>
		</a>

		<!-- Global links -->
		<nav class="rj-h-part rj-h-part_top rj-h-part_global" >
			<?php rj_global_menu("global-menu"); ?>
			<?php if ( !rj_is_blog() ): ?>
			<ul class="rj-menu rj-menu_global rj-menu_inline">
				<li class="rj-menu-item">
					<a class="rj-menu-item-link rj-responsive-menu-trigger" aria-label="<?= translate("Display the mobile navigation","rokjedna");?>" href="#rjMenu">
						<span class="dashicons dashicons-menu-alt"></span>
					</a>
				</li>
			</ul>
			<?php endif; ?>
			
		</nav>

		<?= rj_site_title(); ?>
		<?php if( !rj_is_blog() ): ?>
		<?php rj_main_menu("main-menu"); ?>
		<?php endif; ?>

	</header>

	<main id="rjMain">
