<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

?>

<?php 
	$slug = rj_page_section_slug();
	if( $slug ) : 
?>
<style>
	.rj-page-section-item_<?= rj_page_section_slug(); ?> .rj-page-section-name,
	.rj-page-section-item_<?= rj_page_section_slug(); ?> .rj-post-list-item-title a:hover,
	.rj-page-section-item_<?= rj_page_section_slug(); ?> .rj-post-list-item-title a:focus
	{
		color: <?= rj_page_section_color(); ?>
	}
</style>
<?php endif; ?>

<article id="post-<?php the_ID(); ?>" class="rj-post-list-item <?= $slug ? "rj-page-section-item_".rj_page_section_slug() : false; ?>" role="listitem" aria-atomic="true">
	<div class="rj-post-list-item-wrap">
		<header class="entry-header">
			<?php the_title( sprintf( '<h2 class="rj-post-list-item-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

			<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php
				rokjedna_posted_on();
				?>
			</div><!-- .entry-meta -->
			<?php endif; ?>

			<?php if ( $slug && $title = rj_page_section_title() != get_the_title() ) : ?>
				<div class="rj-post-list-item-footer">
					<span class="rj-post-list-item-label">V sekci</span> <span class="rj-page-section-name"><?= rj_page_section_title(); ?><span>
				</div>
			<?php endif; ?>

		</header><!-- .entry-header -->

		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->

	</div>

	
</article><!-- #post-<?php the_ID(); ?> -->
