<?php
/**
 * Page template formatter for the homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Rokjedna
 */

 $c = new FrontCarouselManager();

?>



<!-- The Bootstrap carousel of recent posts -->
<section class="rj-section rj-section_carousel">

	<div id="carouselFrontIndicators" class="carousel slide rj-carousel" data-ride="carousel" data-interval="5000" data-pause="false">
		<ol class="carousel-indicators rj-carousel-indicators">
			<?php $c->render_dots(); ?>
		</ol>
		<div class="carousel-inner rj-carousel-inner">
			<?php $c->render_slides(); ?>
		</div>
	</div>

</section>

<!-- Navigace na stránce -->
<?php rj_page_menu("main-menu"); ?>

<!-- Měsíční checklist -->
<section class="rj-section rj-section_ch rj-ch" aria-labelledby="rjChDesc">
	<h2 class="rj-ch-title" id="rjChDesc"><?= translate("Have it okay","rokjedna"); ?></h2>
	<?php rj_checklists(); ?>
</section>


	
	

	<div class="rj-post-content rj-gutenberg <?php rj_post_gutter_classes( get_post() ); ?>">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="rj-page-links">' . esc_html__( 'Pages:', 'rokjedna' ),
				'after'  => '</div>',
			)
		);
		?>
	</div>
