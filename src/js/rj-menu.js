import "jquery";

$(document).ready(function(){

  let menuObject = $(".rj-h-part_main-menu");


  function setMenuStatus( $status ){

    if ( $status ) {

      menuObject.addClass("rj-h-part_main-menu_expanded");

    } else {

      menuObject.removeClass("rj-h-part_main-menu_expanded");

    }

  }

  function getMenuStatus(){

    if ( menuObject.hasClass("rj-h-part_main-menu_expanded") ) {
      return true;
    } else {
      return false;
    }

  }

  $("#rjMenuCloser").on("click", function(){
    setMenuStatus( false );
  });

  $("#rjMenuCloser").blur(function(){
    setMenuStatus( false );
  });

  $(".rj-responsive-menu-trigger").on("click",function(){
    setMenuStatus( true );
  });




  $("#rjGoToMenu").on("click",function(){

    // Set focus to the first element
    let links = $("#rjMenu a");

    if ( links.length > 0 ) {

      let id = $( links[0] ).attr("id");
      let target = $("#"+id);

      target.focus();

    }

    // activate the menu by class
    setMenuStatus( true );




  });



  $(".rj-menu-item-expander").on("click", function(){

    let section = $(this).closest(".menu-item");

    console.log(section);

    if ( section.hasClass("rj-menu-item_closed") ) {
      // console.log("Je to zavřené");
      setSectionState(true);
    } else {
      setSectionState(false);
    }

    function setSectionState($state){
    
      if ( $state ) {
        section.addClass("rj-menu-item_expanded");
        section.removeClass("rj-menu-item_closed");
        section.attr("aria-expanded", "true");
      } else {
        section.removeClass("rj-menu-item_expanded");
        section.addClass("rj-menu-item_closed");
        section.attr("aria-expanded", "false");
      }

    }

  });

  $(".rj-responsive-menu-trigger").on("click", function(){

    const menu = $("#rj_navigation");
    console.log(menu);

    function setMenuState($state){

      if ($state) {
        menu.removeClass("rj-responsive-menu_collapsed");
        menu.addClass("rj-responsive-menu_expanded");
        menu.attr("aria-expanded", "true");
        $("body").addClass("rj-menufix");
        let links = $(".rj-menu_header .menu-item");
        $(links[0]).focus();
      } else {
        menu.addClass("rj-responsive-menu_collapsed");
        menu.removeClass("rj-responsive-menu_expanded");
        menu.attr("aria-expanded", "false");
        $("body").removeClass("rj-menufix");
      }

    }

    if (menu.hasClass("rj-responsive-menu_collapsed")) {
      setMenuState(true);
    } else {
      setMenuState(false);
    }

  });

});