const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require('webpack');

module.exports = {
  entry: ['./src/index.js','./src/scss/rokjedna.scss','./src/scss/editor.scss'],
  output: {
    path: path.resolve(__dirname, 'assets'),
    filename: 'rokjedna.js'
  },
  externals: {
    jquery: 'jQuery'
  },
  mode: 'development',
  module: {
    rules: [
      // JS through babel
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      // SCSS
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          // { loader: MiniCssExtractPlugin.loader },
          {
            loader: 'file-loader',
            options: {
                name: '[name].css',
                context: './',
                outputPath: '/',
                publicPath: '/dist'
            }
          },
          {
            loader: 'extract-loader'
          },
          { loader: 'css-loader' },
          { loader: 'postcss-loader' },
          { 
            loader: 'sass-loader',  
            options: {
              implementation: require('sass'),
            }
          }
        ]
      },
      // Images
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              outputPath: 'images',
              name: '[name].[ext]',
              context: 'src'
            }
          }
        ]
      },
      // Fonts
      {
        test: /\.(ttf|woff|woff2|svg|eot)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              outputPath: 'fonts',
              name: '[path][name].[ext]',
              context: 'src'
            }
          }
        ]
      },
      // End of modules
    ]
  },
  plugins: [
    new CleanWebpackPlugin({

    }),
    new MiniCssExtractPlugin({
      filename: '[name].css'
      // moduleFilename: ({ name }) => `${name.replace('/js/', '/css/')}.css`,
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
  ],
};